AUTOBAHN_DEBUG = false;

var Datalake = function (key,url,app,sec) {
    this.key = key;
    this.url = url;
    this.app = app;
    this.sec = sec;
};

Datalake.prototype.request = function (method,address,payload,success,error) {
    jQuery.ajax({
        //data
        contentType: "application/json",
        processData: false,
        dataType: 'json',

        //action
        url: this.url+address,
        type: method,
        data: payload,

        //authentication
        headers: {
            "X-Parse-Application-Id": this.app,
            "X-Parse-REST-API-Key": this.sec,
        },
        success: success,
        error: error,
    });
};

Datalake.prototype.list = function (success,error) {
    this.request("GET","schemas","",success,error);
};

Datalake.prototype.read = function (alias,success,error) {
    this.request("GET","schemas/"+alias,"",success,error);
};

Datalake.prototype.make = function (alias,field,success,error) {
    this.request("POST","schemas/"+alias,{
        "fields": {
            "name": { "type": "String" },
        },
        "className": alias,
    },success,error);
};

Datalake.prototype.grid = function (alias) {
    if (!(this.sch[alias])) {
        this.sch[alias] = new Datagrid(this,alias);
    }
    return this.sch[alias];
};

/******************************************************************************/

var Datagrid = function (lake,name) {
    this.lake = lake;
    this.name = name;
};

Datagrid.prototype.list = function (success,error) {
    this.lake.request("GET","classes/"+this.name,"",success,error);
};

/******************************************************************************/

var Endpoint = {
    plug: function (key,url,app,sec) {
        Endpoint.grid[key] = new Datalake(key,url,app,sec);

        return Endpoint.grid[key];
    },
    grid: {},
    view: function (source,context) {
        var template = Twig.twig({
            data: $(source).html(),
        });

        resp = template.render(context);

        return resp;
    },
    load: function (source,target,callback,behavior) {
        var tpl = $(source).html();

        if (callback) {
            tpl = callback(tpl);
        }

        switch (behavior) {
            case 'prepend':
                $(target).prepend(tpl);
                break;
            case 'append':
                $(target).append(tpl);
                break;
            default:
                $(target).html(tpl);
                break;
        }
    },
    ajax: function (acte,args,hook,fail,encode) {
        var link = localStorage['endpoint'];

        $.ajax({
            url: Endpoint.conn.link+"/?acte="+acte+"&"+args,
            dataType: encode || "json",
            success: hook,
            error: fail,
        });
    },
    name: function (alias,value) {
        if (!(Endpoint.vars[alias])) {
            Endpoint.vars[alias] = value;
        }
        return Endpoint.vars[alias];
    },
    init: function (alias,value) {
        if (!(Endpoint.args[alias])) {
            Endpoint.args[alias] = value;
        }
        return Endpoint.args[alias];
    },
    vars: {
    },
    args: {},
    conn: null
};

Endpoint.conn = JSON.parse(localStorage['endpoint'] || '{}');

if (!(Endpoint.conn.host)) Endpoint.conn.host = 'localhost';
if (!(Endpoint.conn.port)) Endpoint.conn.port = 3000;
if (!(Endpoint.conn.type)) Endpoint.conn.type = 'http';

Endpoint.conn.link = Endpoint.conn.type+'://'+Endpoint.conn.host+':'+Endpoint.conn.port;

jQuery(function ($) {
    var qs = window.location.search.substring(1).split('&');

    for (i=0 ; i<qs.length ; i++) {
        pair = qs[i].split('=');

        Endpoint.args[pair[0]] = pair[1];
    }

    $('header input').keypress(function (ev) {
        if (ev.originalEvent.charCode==13) {
            var word = $(ev.currentTarget).val();
            var link = word.split(' ').join('+');

            //console.log('Searching "'+word+'": ',"/finding.html?q="+link);

            window.location.replace("/finding.html?q="+link);
        }
    });

    $('.dropdown-toggle').each(function (idx,obj) {
        $(obj).dropdown();
    });

    $.ajax({
        success: function (source) {
            $('#sidebarMenu').html(source);

            $('#sidebarMenu a').each(function (idx,elm) {
                if ($(elm).attr('href')==window.location.pathname) {
                    if ($(elm).hasClass('nav-link')) {
                        $(elm).addClass('active');
                    } else if ($(elm).hasClass('link-secondary')) {
                        $(elm).addClass('link-primary').removeClass('link-secondary');
                    }
                }
            });
        },
        url: "/engin/tpl/sidebar.html",
    });

    setTimeout(config_refresh, 1000);
});

function config_refresh () {
  $("input[data-config],textareas[data-config]").change(function (ev) {
      var k = $(ev.currentTarget).attr('data-config');

      var v = $(ev.currentTarget).val();

      localStorage[k] = JSON.stringify(v);
  });
  $("select[data-config]").change(function (ev) {
      var k = $(ev.currentTarget).attr('data-config');

      var v = $(ev.currentTarget).val();

      localStorage[k] = JSON.stringify(v);
  });
  $("input[data-config],textareas[data-config],select[data-config]").each(function (idx,elm) {
      var k = $(elm).attr('data-config'), v;

      try {
        v = JSON.parse(localStorage[k]);
      } catch (ex) {
        v = null;
      }

      $(elm).val(v);

      if ($(elm).hasClass('chosen-select')) {
        $(elm).chosen();

        //$(elm).trigger("chosen:updated");
      }
  });

}

function replace_all (origin,source,target) {
    var result = origin;
    var number = 0;

    while (result.search(source) && number<10) {
        result = result.replace(source,target); number += 1;
    }

    return result;
}

function highlight(name) {
    var obj = document.getElementById(name);

    if (obj.value.trim().length==0) {
        alert("Empty field : "+name);

        obj.focus();

        return false;
    }

    return true;
}

function add_binaural (alias,entry) {
    /*
    var entry = {
        type:'success', icon:'bed', name:"delta", list:[
            "Dreamless",
            "NREM sleep",
            "Pain relief",
            "Access the subconscious",
            "Loss of bodily awareness",
        ], func: function (today,value,range) {
            return Math.random() * 100;
        }, size:5, rank:[0,4], help:"ooooooooooooooooooo"
    };//*/

    entry.name = alias;
    entry.curr = Math.floor((entry.rank[0] + entry.rank[1]) / 2);
    entry.size = Math.round(entry.rank[1] - entry.rank[0]);

    Endpoint.vars.rhythm.binaurals.push(entry);
}

Endpoint.name('rhythm',{
    circadien: {
    },
    binaurals: [],
});

add_binaural("delta",{
    type:'success', icon:'bed', list:[
        "Dreamless",
        "NREM sleep",
        "Pain relief",
        "Subconsciousness",
        "Bodily Unawareness",
    ], func: function (today,value,range) {
        var entry = [], value;

        for (i=0 ; i<value.length ; i++) {
            value = Math.random() * 100;

            entry.push(value);
        }

        return entry;
    }, size:5, rank:[0,4], help:"ooooooooooooooooooo"
});

add_binaural("theta",{
    type:'secondary', icon:'balance-scale', list:[
        "Inner peace",
        "REM sleep",
        "Deep meditation",
        "Addiction help",
        "Healing",
    ], func: function (today,value,range) {
        var entry = [], value;

        for (i=0 ; i<value.length ; i++) {
            value = Math.random() * 100;

            entry.push(value);
        }

        return entry;
    }, size:5, rank:[4,7], help:"ooooooooooooooooooo"
});

add_binaural("alpha",{
    type:'primary', icon:'eye', list:[
        "Flow state",
        "Creativity",
        "Focus",
        "Learning",
        "Serotonin boost",
    ], func: function (today,value,range) {
        var entry = [], value;

        for (i=0 ; i<value.length ; i++) {
            value = Math.random() * 100;

            entry.push(value);
        }

        return entry;
    }, size:5, rank:[7,13], help:"ooooooooooooooooooo"
});

add_binaural("beta",{
    type:'warning', icon:'bolt', list:[
        "Arousal",
        "Concentration",
        "Alertness",
        "Motivation",
        "Problem solving",
    ], func: function (today,value,range) {
        var entry = [], value;

        for (i=0 ; i<value.length ; i++) {
            value = Math.random() * 100;

            entry.push(value);
        }

        return entry;
    }, size:5, rank:[13,39], help:"ooooooooooooooooooo"
});

add_binaural("gamma",{
    type:'info', icon:'globe', list:[
        "Self-control",

        "Eureka !",
        "Peak awareness",
        "Intelligence",
        "Feeling of oneness",
    ], func: function (today,value,range) {
        var entry = [], value;

        for (i=0 ; i<value.length ; i++) {
            value = Math.random() * 100;

            entry.push(value);
        }

        return entry;
    }, size:5, rank:[40,70], help:"ooooooooooooooooooo"
});

add_binaural("heart",{
    type:'danger', icon:'heartbeat', list:[
        "oooooooooo",
        "oooooooooo",
    ], func: function (today,value,range) {
        var entry=[], value;

        for (i=0 ; i<value.length ; i++) {
            value = Math.random() * 100;

            entry.push(value);
        }

        return entry;
    }, size:5, rank:[25,250], help:"ooooooooooooooooooo"
});

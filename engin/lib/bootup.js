var Warehouse = {
    cache: {},
    store: {},
    parse: {},
    graph: {},
    neo4j: {},
    vault: {},
};

/*******************************************************************************/

var Dependency = {
    web: {
        name: "Gentellela !",
        icon: "paw",
        link: "/landing.html",
    },
    nav: {
        side: [{
            icon: "paw", name: "General", unit: [{
/*
                icon: "home", name: "Dashboard", list: [{
                    link: "/theme/layout-dash1.html", icon: "gauge", name: "Dashpanel 1"
                },{
                    link: "/theme/layout-dash2.html", icon: "gauge", name: "Dashpanel 2"
                },{
                    link: "/theme/layout-dash3.html", icon: "gauge", name: "Dashpanel 3"
                }],
            },{
//*/
                icon: "leaf", name: "Modeling", list: [{
                    link: "/model/agent.html", icon: "gauge", name: "Agent"
                },{
                    link: "/model/bills.html", icon: "gauge", name: "Bills"
                },{
                    link: "/model/clock.html", icon: "gauge", name: "Clock"
                },{
                    link: "/model/event.html", icon: "gauge", name: "Event"
                },{
                    link: "/model/inbox.html", icon: "gauge", name: "Inbox"
                },{
                    link: "/model/place.html", icon: "gauge", name: "Place"
                },{
                    link: "/model/topic.html", icon: "gauge", name: "Topic"
                },{
                    link: "/model/vcard.html", icon: "gauge", name: "vCard"
                }],
            },{
                icon: "database", name: "Datasets", list: [{
                    link: "/daten/demos/chartjs.html", icon: "gauge", name: "Chart.js"
                },{
                    link: "/daten/demos/echarts.html", icon: "gauge", name: "eCharts"
                },{
                    link: "/daten/demos/morisjs.html", icon: "gauge", name: "Moris.js"
                },{
                    link: "/daten/demos/pricing.html", icon: "gauge", name: "Pricing"
                },{
                    link: "/daten/demos/retail.html", icon: "gauge", name: "Retail"
                },{
                    link: "/daten/demos/tables.html", icon: "gauge", name: "Tables"
                }],
            },{
                icon: "link", name: "Hyper-Link", list: [{
                    link: "/hyper/cypher.html", icon: "gauge", name: "Neo4j / Cypher"
                },{
                    link: "/hyper/graphql.html", icon: "gauge", name: "GraphQl"
                },{
                    link: "/hyper/sparql.html", icon: "gauge", name: "RDF / SparQL"
                }],
            },{
                icon: "book", name: "Knowledger", list: [{
                    link: "/knows/biology.html", icon: "gauge", name: "Biology"
                },{
                    link: "/knows/concept.html", icon: "gauge", name: "Concept"
                },{
                    link: "/knows/dialect.html", icon: "gauge", name: "Dialect"
                },{
                    link: "/knows/entity.html", icon: "gauge", name: "Entity"
                },{
                    link: "/knows/perceive.html", icon: "gauge", name: "Perceive"
                },{
                    link: "/knows/records.html", icon: "gauge", name: "Records"
                }],
            },{
                icon: "files-o", name: "Watching", list: [{
                    link: "/watch/asset.html", icon: "archive", name: "Asset"
                },{
                    link: "/watch/media.html", icon: "files-o", name: "Media"
                },{
                    link: "/watch/links.html", icon: "link", name: "Links"
                },{
                    link: "/watch/phone.html", icon: "mobile", name: "Phone"
                },{
                    link: "/watch/webtv.html", icon: "tv", name: "WebTV"
                }],
            }],
        }],
        user: [{
            icon: "user", name: "Profile", link: "/setup/perso.html"
        },{
            icon: "cogs", name: "Settings", link: "/setup/index.html"
        }],
    },
    lib: [
        "jquery",
        "moment",
            "js/vue.min",
            "js/popper.min",
            //"js/underscore.min",
        "bootstrap",
            "ext/iCheck/icheck.min",
            //"ext/bootstrap-daterangepicker/daterangepicker",
            "ext/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min",
            "ext/fastclick/lib/fastclick",
            "ext/bootstrap-progressbar/bootstrap-progressbar.min",
            "ext/jquery-sparkline/dist/jquery.sparkline.min",
            "ext/jquery.easy-pie-chart/dist/jquery.easypiechart.min",
            "ext/dropzone/dist/min/dropzone.min",
        "raphael",
        "echarts",
        "ext/echarts/map/js/world",
        "ext/morris.js/morris.min",
            "ext/fullcalendar/dist/fullcalendar.min",
            /*
            "ext/datatables.net/js/jquery.dataTables.min",
            "ext/datatables.net-bs/js/dataTables.bootstrap.min",
            "ext/datatables.net-buttons/js/dataTables.buttons.min",
            "ext/datatables.net-buttons-bs/js/buttons.bootstrap.min",
            "ext/datatables.net-buttons/js/buttons.flash.min",
            "ext/datatables.net-buttons/js/buttons.html5.min",
            "ext/datatables.net-buttons/js/buttons.print.min",
            "ext/datatables.net-fixedheader/js/dataTables.fixedHeader.min",
            "ext/datatables.net-keytable/js/dataTables.keyTable.min",
            "ext/datatables.net-responsive/js/dataTables.responsive.min",
            "ext/datatables.net-responsive-bs/js/responsive.bootstrap",
            "ext/datatables.net-scroller/js/dataTables.scroller.min",
            //*/
            "ext/jszip/dist/jszip.min",
            "ext/pdfmake/build/pdfmake.min",
            "ext/pdfmake/build/vfs_fonts",
        "js/jquery-ui.custom.min",
        "js/jquery.csv.min",
        "js/jquery.steps.min",
        "js/jquery-qrcode-0.18.0.min",
            "ext/jQuery-Smart-Wizard/js/jquery.smartWizard",
            "ext/jquery.hotkeys/jquery.hotkeys",
            "ext/google-code-prettify/src/prettify",
            "ext/jquery.tagsinput/src/jquery.tagsinput",
            //"ext/switchery/dist/switchery.min",
            //"ext/select2/dist/js/select2.full.min",
            //"ext/parsleyjs/dist/parsley.min",
            "ext/autosize/dist/autosize.min",
            //"ext/ext/devbridge-autocomplete/dist/jquery.autocomplete.min",
            //"ext/starrr/dist/starrr",
        "js/autobahn.min",
        "js/twig.min",
    
        "js/jsonld.min",
        //"js/rdform",
        //"js/semantic.min",
        "js/yasgui.min",
    
        "js/d3-queue",
        "js/telegram-passport",
    
        //"js/backbone.min",
        //"js/LargeLocalStorage.min",
        //"js/parse.min",

        //"https://cdn.jsdelivr.net/npm/@tensorflow/tfjs@2.0.0/dist/tf.min.js",

        "lib/custom/jquery",
            "lib/custom/hovers",
            "lib/custom/resize",
            "lib/custom/notify",
        "lib/custom/bootstrap",
            "lib/custom/chartings",
            "lib/custom/sparkline",
            "lib/custom/datatable",
            "lib/custom/geolocate",
            "lib/custom/interface",
        "lib/custom/weather",
    ],
    ejs: {
        jqueryslim: 'js/jquery.slim.min',
        jquery: 'ext/jquery/dist/jquery.min',
        moment: 'js/moment.min', //"ext/moment/min/moment.min",

        bootstrap: 'ext/bootstrap/dist/js/bootstrap.min',
        //nprogress: 'ext/nprogress/nprogress',

        echarts: "ext/echarts/dist/echarts.min",
        raphael: "ext/raphael/raphael.min",
    },
    vue: {},
    url: "/asset/",
};

/*******************************************************************************/

var Template = {
    genQR: function (uuid,name,text,size=200,opts=null) {
        var opts = opts || {
            radius: 0,
            // corner radius relative to module width: 0.0 .. 0.5
            fill: '#000',
            background: null,
                fontname: 'sans',
                fontcolor: '#000',
            mSize: 0.1,
            mPosX: 0.5,
            mPosY: 0.5,
            // offset in pixel if drawn onto existing canvas
            left: 0,
            top: 0,
            // render method: 'canvas', 'image' or 'div'
            render: 'canvas',
            image: null,
            // error correction level: 'L', 'M', 'Q' or 'H'
            ecLevel: 'L',
            // quiet zone in modules
            quiet: 0,
            mode: 0,
        };

        opts.label = name || 'no label';
        opts.text  = text;
        opts.size  = size || 200;

        $(uuid).qrcode(opts);
    },
    checks: function (uid,cnt) {
        var src = document.getElementById("tpl_"+uid);
        
        if (src!=null) {
            Template.render(uid,cnt,src.innerHTML);
        } else {
            console.log("Missing source for template : '"+uid+"'");
        }
    },
    render: function (uid,cnt,src) {
        var tpl = twig({
            data: src,
        });
        
        var obj = document.getElementById(uid);
        
        if (obj!=null) {
            obj.innerHTML = tpl.render(cnt);
        } else {
            console.log("Missing target '"+uid+"' for template !");
        }
    },
    compoz: function (alias,fields,source) {
        return Vue.component(alias, {
            props: fields,
            template: source,
        });
    },
    shower: function (key,uid,cnt,src) {
        Dependency.vue[key] = new Vue({
            el: uid,
            data: cnt,
            template: src,
        });
    },
    nav: {
        side: '{% for group in sidenav %}<div class="menu_section"><h3>{{ group.name }}</h3>' +
            '<ul class="nav side-menu">{% for unit in group.unit %}<li>' +
            '<a><i class="fa fa-{{ unit.icon }}"></i>{{ unit.name }}' +
            '<span class="fa fa-chevron-down"></span></a><ul class="nav child_menu">' +
            '{% for menu in unit.list %}<li><a href="{{ menu.link }}">'+
            '<i class="fa fa-{{ menu.icon }}"></i>&nbsp;{{ menu.name }}</a></li>{% endfor %}'+
            '</ul></li>{% endfor %}</ul></div>{% endfor %}',
        user: '<li><a href="/setup/perso.html"> Profile</a></li>' +
            '<li><a href="/setup/index.html"><span class="badge bg-red pull-right">50%</span>' +
            '<span>Settings</span></a></li>' +
            '<li><a href="javascript:;">Help</a></li>' +
            '<li><a class="auth_logout"><i class="fa fa-sign-out pull-right"></i>Log Out</a></li>',
        mail: '{% for msg in mailbox %}<li><a>' +
            '<span class="image"><img src="{{ msg.image }}" alt="{{ msg.title }}" /></span>' +
            '<span><span>{{ msg.title }}</span><span class="time">{{ msg.since }}</span></span>' +
            '<span class="message">{{ msg.entry }}</span></a></li>{% endfor %}',
    },
    handle: [],
};

/*******************************************************************************/

requirejs.config({
    baseUrl: Dependency.url,
    paths: Dependency.ejs,
});

requirejs(Dependency.lib, function () {
    requirejs([
        "lib/custom",
        "lib/moteur",
    ], function(moteur) {
        jQuery(document).ready(function ($) {
            for (var i=0 ; i<Template.handle.length ; i++) {
                Template.handle[i]($);
            }
    		init_sparklines();
    		init_flot_chart();
    		init_sidebar();
    		init_wysiwyg();
    		init_InputMask();
    		init_JQVmap();
    		init_cropper();
    		init_knob();
    		init_IonRangeSlider();
    		init_ColorPicker();
    		init_TagsInput();
    		init_parsley();
    		init_daterangepicker();
    		init_daterangepicker_right();
    		init_daterangepicker_single_call();
    		init_daterangepicker_reservation();
    		init_SmartWizard();
    		init_EasyPieChart();
    		init_charts();
    		init_echarts();
    		//init_morris_charts();
    		init_skycons();
    		init_select2();
    		init_validator();
    		init_DataTables();
    		init_chart_doughnut();
    		init_gauge();
    		init_PNotify();
    		init_starrr();
    		init_calendar();
    		init_compose();
    		init_CustomNotification();
    		init_autosize();
    		init_autocomplete();
        });
    });
});

var Security = {
    post: function (ev) {
        localStorage["current_user"] = "tayamino";
        
        ev.preventDefault();
        
        return false;
    },
    quit: function (ev) {
        localStorage["current_user"] = "anonymous";
        
        //window.location.replace("/setup/login.html");
        
        ev.preventDefault();
        
        window.location.replace("/setup/index.html");
        
        return false;
    },
    init: function ($) {
        $(".auth_logout").click(Security.quit);
        $("form.auth_login").submit(Security.post);

        if (localStorage["current_user"]==undefined || localStorage["current_user"]==null) {
            localStorage["current_user"] = "anonymous";
        }
        
        Security.curr = Security.find(localStorage["current_user"]) || Security.find("john-doe");
    
        localStorage["current_user"] = Security.curr.alias;
    },
    show: function ($) {
        $("#topmenu i").addClass("fa-"+Dependency.web.icon);
        $("#topmenu span").html(Dependency.web.name);
        $("#topmenu a").attr("href",Dependency.web.link);
    
        $(".whoami").html(Security.curr.title);

        $("#topinfo img").attr("alt",Security.curr.title);
        $("#topinfo img").attr("src",Security.curr.image);

        $("#usernav img").attr("alt",Security.curr.title);
        $("#usernav img").attr("src",Security.curr.image);
    },
    auth: function (pseudo,passwd) {
        var curr = Security.find(pseudo);
        
        if (curr!=null) {
            if (curr.passwd==passwd) {
                localStorage["current_user"] = curr.alias;
            }
        }
        
        window.location.replace("/setup/index.html");
    },
    find: function (alias) {
        var resp = null;
        
        for (var i=0 ; i<Security.user.length ; i++) {
            if (alias==Security.user[i].alias) {
                resp = Security.user[i];
            }
        }
        
        return resp;
    },
    mail: [{
        title: "John Doe",
        image: "/asset/img/mugshot/anonymous.png",
        entry: "Film festivals used to be do-or-die moments for movie makers. They were where...",
        since: "3 mins ago",
    }],
    role: [{
        alias: "john-doe",
        title: "John Doe",
    }],
    user: [{
        alias: "anonymous",
        title: "Not logged",
        email: "no-reply@localhost",
        phone: "06xxxxxxxxx",
        image: "/asset/img/mugshot/anonymous.png",
        cover: "/asset/img/flagify.png",
    },{
        alias: "john-doe",
        title: "John Doe",
        email: "john@doe.me",
        phone: "06123456789",
        image: "/asset/img/mugshot/john-doe.jpg",
        cover: "/asset/img/flagify.png",
    },{
        alias: "tayamino",
        title: "TAYAA Med Amine",
        email: "tayamino@gmail.com",
        phone: "0644764608",
        image: "/asset/img/mugshot/tayamino.jpg",
        cover: "/asset/img/flagify.png",
    }],
    curr: null,
};

Template.handle.push(function ($) {
    Security.init($);
    
    Security.show($);
});

Template.handle.push(function ($) {
    
});

Template.handle.push(function ($) {
    var context = {
        // alias: "value",
    };

    context.sidenav = Dependency.nav.side;
    context.usernav = Dependency.nav.user;

    context.website = Dependency.web;

    context.profile = Security.curr;
    context.mailbox = Security.mail;
    
    Template.render("sidenav",       context, Template.nav.side);
    if ($("#mail_list").length) {
        Template.render("mail_list", context, Template.nav.mail);
    } else {
        Template.render("menu1",     context, Template.nav.mail);
    }
    Template.render("user_menu",     context, Template.nav.user);
    
    var target = [
        "profile_card","profile_data",
    ];
    
    for (var i=0 ; i<target.length ; i++) {
        Template.checks(target[i],context);
    }
});

Template.handle.push(function ($) {
    jQuery("div.sparql-gui").each(function (idx,elm) {
        var cfg = {
            requestConfig: {
                endpoint: jQuery(elm).attr("data-url") || "http://example.com/sparql",
            },
            copyEndpointOnNewTab: false
        };
        
        var obj = new Yasgui(elm, cfg);
    });
});

Template.handle.push(function ($) {
    
});

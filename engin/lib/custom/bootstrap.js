// NProgress
if (typeof NProgress != 'undefined') {
    $(document).ready(function () {
        NProgress.start();
    });

    $(window).load(function () {
        NProgress.done();
    });
}

// Tooltip
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
});
// /Tooltip

// Progressbar
if ($(".progress .progress-bar")[0]) {
    $('.progress .progress-bar').progressbar();
}
// /Progressbar

// STARRR
		
function init_starrr() {
	
	if( typeof (starrr) === 'undefined'){ return; }
	console.log('init_starrr');
	
	$(".stars").starrr();

	$('.stars-existing').starrr({
	  rating: 4
	});

	$('.stars').on('starrr:change', function (e, value) {
	  $('.stars-count').html(value);
	});

	$('.stars-existing').on('starrr:change', function (e, value) {
	  $('.stars-count-existing').html(value);
	});
	
  };
	
// Accordion
$(document).ready(function() {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});

// Switchery
$(document).ready(function() {
    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A'
            });
        });
    }
});
// /Switchery


// iCheck
$(document).ready(function() {
    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
});
// /iCheck

	  /* VALIDATOR */

	  function init_validator () {
		 
		if( typeof (validator) === 'undefined'){ return; }
		console.log('init_validator');
	  
	  // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
		});
	  
	  };
	   
	  	/* PNotify */
			
		function init_PNotify() {
			
			if( typeof (PNotify) === 'undefined'){ return; }
			console.log('init_PNotify');
			
			new PNotify({
			  title: "PNotify",
			  type: "info",
			  text: "Welcome. Try hovering over me. You can click things behind me, because I'm non-blocking.",
			  nonblock: {
				  nonblock: true
			  },
			  addclass: 'dark',
			  styling: 'bootstrap3',
			  hide: false,
			  before_close: function(PNotify) {
				PNotify.update({
				  title: PNotify.options.title + " - Enjoy your Stay",
				  before_close: null
				});

				PNotify.queueRemove();

				return false;
			  }
			});

		};

/* PARSLEY */
	
function init_parsley() {
	
	if( typeof (parsley) === 'undefined'){ return; }
	console.log('init_parsley');
	
	$/*.listen*/('parsley:field:validate', function() {
	  validateFront();
	});
	$('#demo-form .btn').on('click', function() {
	  $('#demo-form').parsley().validate();
	  validateFront();
	});
	var validateFront = function() {
	  if (true === $('#demo-form').parsley().isValid()) {
		$('.bs-callout-info').removeClass('hidden');
		$('.bs-callout-warning').addClass('hidden');
	  } else {
		$('.bs-callout-info').addClass('hidden');
		$('.bs-callout-warning').removeClass('hidden');
	  }
	};
  
	$/*.listen*/('parsley:field:validate', function() {
	  validateFront();
	});
	$('#demo-form2 .btn').on('click', function() {
	  $('#demo-form2').parsley().validate();
	  validateFront();
	});
	var validateFront = function() {
	  if (true === $('#demo-form2').parsley().isValid()) {
		$('.bs-callout-info').removeClass('hidden');
		$('.bs-callout-warning').addClass('hidden');
	  } else {
		$('.bs-callout-info').addClass('hidden');
		$('.bs-callout-warning').removeClass('hidden');
	  }
	};
	
	  try {
		hljs.initHighlightingOnLoad();
	  } catch (err) {}
	
};


  /* INPUTS */
  
	function onAddTag(tag) {
		alert("Added a tag: " + tag);
	  }

	  function onRemoveTag(tag) {
		alert("Removed a tag: " + tag);
	  }

	  function onChangeTag(input, tag) {
		alert("Changed a tag: " + tag);
	  }

	  //tags input
	function init_TagsInput() {
		  
		if(typeof $.fn.tagsInput !== 'undefined'){
		 
		$('#tags_1').tagsInput({
		  width: 'auto'
		});
		
		}
		
    };


	   	/* CALENDAR */
		  
		    function  init_calendar() {
					
				if( typeof ($.fn.fullCalendar) === 'undefined'){ return; }
				console.log('init_calendar');
					
				var date = new Date(),
					d = date.getDate(),
					m = date.getMonth(),
					y = date.getFullYear(),
					started,
					categoryClass;

				var calendar = $('#calendar').fullCalendar({
				  header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay,listMonth'
				  },
				  selectable: true,
				  selectHelper: true,
				  select: function(start, end, allDay) {
					$('#fc_create').click();

					started = start;
					ended = end;

					$(".antosubmit").on("click", function() {
					  var title = $("#title").val();
					  if (end) {
						ended = end;
					  }

					  categoryClass = $("#event_type").val();

					  if (title) {
						calendar.fullCalendar('renderEvent', {
							title: title,
							start: started,
							end: end,
							allDay: allDay
						  },
						  true // make the event "stick"
						);
					  }

					  $('#title').val('');

					  calendar.fullCalendar('unselect');

					  $('.antoclose').click();

					  return false;
					});
				  },
				  eventClick: function(calEvent, jsEvent, view) {
					$('#fc_edit').click();
					$('#title2').val(calEvent.title);

					categoryClass = $("#event_type").val();

					$(".antosubmit2").on("click", function() {
					  calEvent.title = $("#title2").val();

					  calendar.fullCalendar('updateEvent', calEvent);
					  $('.antoclose2').click();
					});

					calendar.fullCalendar('unselect');
				  },
				  editable: true,
				  events: [{
					title: 'All Day Event',
					start: new Date(y, m, 1)
				  }, {
					title: 'Long Event',
					start: new Date(y, m, d - 5),
					end: new Date(y, m, d - 2)
				  }, {
					title: 'Meeting',
					start: new Date(y, m, d, 10, 30),
					allDay: false
				  }, {
					title: 'Lunch',
					start: new Date(y, m, d + 14, 12, 0),
					end: new Date(y, m, d, 14, 0),
					allDay: false
				  }, {
					title: 'Birthday Party',
					start: new Date(y, m, d + 1, 19, 0),
					end: new Date(y, m, d + 1, 22, 30),
					allDay: false
				  }, {
					title: 'Click for Google',
					start: new Date(y, m, 28),
					end: new Date(y, m, 29),
					url: 'http://google.com/'
				  }]
				});
				
			};

		/* COMPOSE */
		
		function init_compose() {
		
			if( typeof ($.fn.slideToggle) === 'undefined'){ return; }
			console.log('init_compose');
		
			$('#compose, .compose-close').click(function(){
				$('.compose').slideToggle();
			});
		
		};
	   
		/* COLOR PICKER */
			 
		function init_ColorPicker() {
			
			if( typeof ($.fn.colorpicker) === 'undefined'){ return; }
			console.log('init_ColorPicker');
			
				$('.demo1').colorpicker();
				$('.demo2').colorpicker();

				$('#demo_forceformat').colorpicker({
					format: 'rgba',
					horizontal: true
				});

				$('#demo_forceformat3').colorpicker({
					format: 'rgba',
				});

				$('.demo-auto').colorpicker();
			
		};
	   
	   
		/* ION RANGE SLIDER */
			
		function init_IonRangeSlider() {
			
			if( typeof ($.fn.ionRangeSlider) === 'undefined'){ return; }
			console.log('init_IonRangeSlider');
			
			$("#range_27").ionRangeSlider({
			  type: "double",
			  min: 1000000,
			  max: 2000000,
			  grid: true,
			  force_edges: true
			});
			$("#range").ionRangeSlider({
			  hide_min_max: true,
			  keyboard: true,
			  min: 0,
			  max: 5000,
			  from: 1000,
			  to: 4000,
			  type: 'double',
			  step: 1,
			  prefix: "$",
			  grid: true
			});
			$("#range_25").ionRangeSlider({
			  type: "double",
			  min: 1000000,
			  max: 2000000,
			  grid: true
			});
			$("#range_26").ionRangeSlider({
			  type: "double",
			  min: 0,
			  max: 10000,
			  step: 500,
			  grid: true,
			  grid_snap: true
			});
			$("#range_31").ionRangeSlider({
			  type: "double",
			  min: 0,
			  max: 100,
			  from: 30,
			  to: 70,
			  from_fixed: true
			});
			$(".range_min_max").ionRangeSlider({
			  type: "double",
			  min: 0,
			  max: 100,
			  from: 30,
			  to: 70,
			  max_interval: 50
			});
			$(".range_time24").ionRangeSlider({
			  min: +moment().subtract(12, "hours").format("X"),
			  max: +moment().format("X"),
			  from: +moment().subtract(6, "hours").format("X"),
			  grid: true,
			  force_edges: true,
			  prettify: function(num) {
				var m = moment(num, "X");
				return m.format("Do MMMM, HH:mm");
			  }
			});
			
		};
	   
	   
	   /* WYSIWYG EDITOR */

		function init_wysiwyg() {
			
		if( typeof ($.fn.wysiwyg) === 'undefined'){ return; }
		console.log('init_wysiwyg');
			
        function init_ToolbarBootstrapBindings() {
          var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'
            ],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
          $.each(fonts, function(idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
          });
          $('a[title]').tooltip({
            container: 'body'
          });
          $('.dropdown-menu input').click(function() {
              return false;
            })
            .change(function() {
              $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
            })
            .keydown('esc', function() {
              this.value = '';
              $(this).change();
            });

          $('[data-role=magic-overlay]').each(function() {
            var overlay = $(this),
              target = $(overlay.data('target'));
            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
          });

          if ("onwebkitspeechchange" in document.createElement("input")) {
            var editorOffset = $('#editor').offset();

            $('.voiceBtn').css('position', 'absolute').offset({
              top: editorOffset.top,
              left: editorOffset.left + $('#editor').innerWidth() - 35
            });
          } else {
            $('.voiceBtn').hide();
          }
        }

        function showErrorAlert(reason, detail) {
          var msg = '';
          if (reason === 'unsupported-file-type') {
            msg = "Unsupported format " + detail;
          } else {
            console.log("error uploading file", reason, detail);
          }
          $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

       $('.editor-wrapper').each(function(){
			var id = $(this).attr('id');	//editor-one
			
			$(this).wysiwyg({
				toolbarSelector: '[data-target="#' + id + '"]',
				fileUploadError: showErrorAlert
			});
		});
 
		
        window.prettyPrint;
        prettyPrint();
	
    };
	  
	 
/* INPUT MASK */
	
function init_InputMask() {
	
	if( typeof ($.fn.inputmask) === 'undefined'){ return; }
	console.log('init_InputMask');
	
		$(":input").inputmask();
		
};
	  
/* CROPPER */

function init_cropper() {
	
	
	if( typeof ($.fn.cropper) === 'undefined'){ return; }
	console.log('init_cropper');
	
	var $image = $('#image');
	var $download = $('#download');
	var $dataX = $('#dataX');
	var $dataY = $('#dataY');
	var $dataHeight = $('#dataHeight');
	var $dataWidth = $('#dataWidth');
	var $dataRotate = $('#dataRotate');
	var $dataScaleX = $('#dataScaleX');
	var $dataScaleY = $('#dataScaleY');
	var options = {
		  aspectRatio: 16 / 9,
		  preview: '.img-preview',
		  crop: function (e) {
			$dataX.val(Math.round(e.x));
			$dataY.val(Math.round(e.y));
			$dataHeight.val(Math.round(e.height));
			$dataWidth.val(Math.round(e.width));
			$dataRotate.val(e.rotate);
			$dataScaleX.val(e.scaleX);
			$dataScaleY.val(e.scaleY);
		  }
		};


	// Tooltip
	$('[data-toggle="tooltip"]').tooltip();


	// Cropper
	$image.on({
	  'build.cropper': function (e) {
		console.log(e.type);
	  },
	  'built.cropper': function (e) {
		console.log(e.type);
	  },
	  'cropstart.cropper': function (e) {
		console.log(e.type, e.action);
	  },
	  'cropmove.cropper': function (e) {
		console.log(e.type, e.action);
	  },
	  'cropend.cropper': function (e) {
		console.log(e.type, e.action);
	  },
	  'crop.cropper': function (e) {
		console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);
	  },
	  'zoom.cropper': function (e) {
		console.log(e.type, e.ratio);
	  }
	}).cropper(options);


	// Buttons
	if (!$.isFunction(document.createElement('canvas').getContext)) {
	  $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
	}

	if (typeof document.createElement('cropper').style.transition === 'undefined') {
	  $('button[data-method="rotate"]').prop('disabled', true);
	  $('button[data-method="scale"]').prop('disabled', true);
	}


	// Download
	if (typeof $download[0].download === 'undefined') {
	  $download.addClass('disabled');
	}


	// Options
	$('.docs-toggles').on('change', 'input', function () {
	  var $this = $(this);
	  var name = $this.attr('name');
	  var type = $this.prop('type');
	  var cropBoxData;
	  var canvasData;

	  if (!$image.data('cropper')) {
		return;
	  }

	  if (type === 'checkbox') {
		options[name] = $this.prop('checked');
		cropBoxData = $image.cropper('getCropBoxData');
		canvasData = $image.cropper('getCanvasData');

		options.built = function () {
		  $image.cropper('setCropBoxData', cropBoxData);
		  $image.cropper('setCanvasData', canvasData);
		};
	  } else if (type === 'radio') {
		options[name] = $this.val();
	  }

	  $image.cropper('destroy').cropper(options);
	});


	// Methods
	$('.docs-buttons').on('click', '[data-method]', function () {
	  var $this = $(this);
	  var data = $this.data();
	  var $target;
	  var result;

	  if ($this.prop('disabled') || $this.hasClass('disabled')) {
		return;
	  }

	  if ($image.data('cropper') && data.method) {
		data = $.extend({}, data); // Clone a new one

		if (typeof data.target !== 'undefined') {
		  $target = $(data.target);

		  if (typeof data.option === 'undefined') {
			try {
			  data.option = JSON.parse($target.val());
			} catch (e) {
			  console.log(e.message);
			}
		  }
		}

		result = $image.cropper(data.method, data.option, data.secondOption);

		switch (data.method) {
		  case 'scaleX':
		  case 'scaleY':
			$(this).data('option', -data.option);
			break;

		  case 'getCroppedCanvas':
			if (result) {

			  // Bootstrap's Modal
			  $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

			  if (!$download.hasClass('disabled')) {
				$download.attr('href', result.toDataURL());
			  }
			}

			break;
		}

		if ($.isPlainObject(result) && $target) {
		  try {
			$target.val(JSON.stringify(result));
		  } catch (e) {
			console.log(e.message);
		  }
		}

	  }
	});

	// Keyboard
	$(document.body).on('keydown', function (e) {
	  if (!$image.data('cropper') || this.scrollTop > 300) {
		return;
	  }

	  switch (e.which) {
		case 37:
		  e.preventDefault();
		  $image.cropper('move', -1, 0);
		  break;

		case 38:
		  e.preventDefault();
		  $image.cropper('move', 0, -1);
		  break;

		case 39:
		  e.preventDefault();
		  $image.cropper('move', 1, 0);
		  break;

		case 40:
		  e.preventDefault();
		  $image.cropper('move', 0, 1);
		  break;
	  }
	});

	// Import image
	var $inputImage = $('#inputImage');
	var URL = window.URL || window.webkitURL;
	var blobURL;

	if (URL) {
	  $inputImage.change(function () {
		var files = this.files;
		var file;

		if (!$image.data('cropper')) {
		  return;
		}

		if (files && files.length) {
		  file = files[0];

		  if (/^image\/\w+$/.test(file.type)) {
			blobURL = URL.createObjectURL(file);
			$image.one('built.cropper', function () {

			  // Revoke when load complete
			  URL.revokeObjectURL(blobURL);
			}).cropper('reset').cropper('replace', blobURL);
			$inputImage.val('');
		  } else {
			window.alert('Please choose an image file.');
		  }
		}
	  });
	} else {
	  $inputImage.prop('disabled', true).parent().addClass('disabled');
	}
	
	
};

/* CROPPER --- end */

/* KNOB */

function init_knob() {

		if( typeof ($.fn.knob) === 'undefined'){ return; }
		console.log('init_knob');

		$(".knob").knob({
		  change: function(value) {
			//console.log("change : " + value);
		  },
		  release: function(value) {
			//console.log(this.$.attr('value'));
			console.log("release : " + value);
		  },
		  cancel: function() {
			console.log("cancel : ", this);
		  },
		  /*format : function (value) {
		   return value + '%';
		   },*/
		  draw: function() {

			// "tron" case
			if (this.$.data('skin') == 'tron') {

			  this.cursorExt = 0.3;

			  var a = this.arc(this.cv) // Arc
				,
				pa // Previous arc
				, r = 1;

			  this.g.lineWidth = this.lineWidth;

			  if (this.o.displayPrevious) {
				pa = this.arc(this.v);
				this.g.beginPath();
				this.g.strokeStyle = this.pColor;
				this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
				this.g.stroke();
			  }

			  this.g.beginPath();
			  this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
			  this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
			  this.g.stroke();

			  this.g.lineWidth = 2;
			  this.g.beginPath();
			  this.g.strokeStyle = this.o.fgColor;
			  this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
			  this.g.stroke();

			  return false;
			}
		  }
		  
		});

		// Example of infinite knob, iPod click wheel
		var v, up = 0,
		  down = 0,
		  i = 0,
		  $idir = $("div.idir"),
		  $ival = $("div.ival"),
		  incr = function() {
			i++;
			$idir.show().html("+").fadeOut();
			$ival.html(i);
		  },
		  decr = function() {
			i--;
			$idir.show().html("-").fadeOut();
			$ival.html(i);
		  };
		$("input.infinite").knob({
		  min: 0,
		  max: 20,
		  stopper: false,
		  change: function() {
			if (v > this.cv) {
			  if (up) {
				decr();
				up = 0;
			  } else {
				up = 1;
				down = 0;
			  }
			} else {
			  if (v < this.cv) {
				if (down) {
				  incr();
				  down = 0;
				} else {
				  down = 1;
				  up = 0;
				}
			  }
			}
			v = this.cv;
		  }
		});
		
};

document.addEventListener('DOMContentLoaded', function() {
  var input = document.getElementById('input');
  var output = document.getElementById('output');

  // Remove styling from text
  input.addEventListener('paste', function (e) {
    e.preventDefault();
    var text = e.clipboardData.getData("text/plain");
    document.execCommand("insertHTML", false, text);
  });

  input.addEventListener('keyup', function () {
    getInfo(input.innerText);
    return false;
  });

  getInfo(input.innerText);
});

renderInfo = function (info) {
  var output = document.getElementById('output');
  var entities = document.getElementById('entities');
  var wordFreq = document.getElementById('word-freq');
  var sentimentTable = document.getElementById('sentiment-table');

  // Marked up
  output.innerHTML = info.tagged;

  // Entitiy list
  entities.innerHTML = '';
  info.entity.forEach( function (e) {
    entities.innerHTML += '<li class="tag ' + e.type + '">' + e.name + '</li>';
  } );
  console.log("Entity : ",info.entity);

  // Document info
  document.getElementById('sentences-stat').innerHTML = info.phrase.length;
  document.getElementById('words-stat').innerHTML = info.metric.word;
  document.getElementById('tokens-stat').innerHTML = info.phrase.length + info.metric.word;
  document.getElementById('entities-stat').innerHTML = info.entity.length;

  // Word frequency
  wordFreq.innerHTML = ''
  info.metric.freq.forEach(function (f) {
    wordFreq.innerHTML += '<tr>' +
      '<td>' + f[0] + '</td>' +
      '<td>' + f[1] + '</td>' +
      '</tr>';
  })

  // Sentiment
  sentimentTable.innerHTML = '';
  info.phrase.forEach(function (s) {
    sentimentTable.innerHTML += '<tr>' +
      '<td class="sentence-emoji">' + getSentimentEmoji(s.sentiment) + '</td>' +
      '<td class="sentence-text">' + s.sentence + '</td>' +
      '<td>' + s.sentiment + '</td>' +
      '</tr>';
  })
}

getSentimentEmoji = function (s) {
  if( s > 1 ) return '😃';
  if( s > 0 ) return '😊';
  if( s < -1 ) return '😢';
  if( s < 0 ) return '☹️';
  if( s === 0 ) return '😶';
}

getInfo = function (v) {
  renderInfo(Natural.stce(v));

  return;

  fetch(
    //'https://showcase-serverless.herokuapp.com/pos-tagger',
    '/asset/natural.json',
    {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      //method: 'POST',
      //body: JSON.stringify({sentence: v})
      method: 'GET',
    })
    .then(function (res) {
        var resp = {
        };

        return res.json();
    }).then(function (info) {
      renderInfo(info);
    })


  return;
  // TODO: This is a mock function and should be replaced by an API call
  var info = {
    taggedText: tag(v),
    entities: entities(v),
    documentInfo: documentInfo(v),
    wordFreq: wordFreq(v),
    sentiments: sentiment(v)
  }

  window.setTimeout( function () {
    renderInfo(info);
  }, 2000);

}

import registeryDummyChooser from '/engin/cdn/rdforms/samples/bootstrap/chooser/dummy.js';
import rdfGraph from '/engin/cdn/rdforms/samples/bootstrap/rdf.js'; // import a rdfjson graph

const bundles = [
  ['/engin/cdn/rdforms/samples/bootstrap/templates/dcterms.json'],
  ['/engin/cdn/rdforms/samples/bootstrap/templates/foaf.json'],
  ['/engin/cdn/rdforms/samples/bootstrap/templates/skos.json'],
  ['/engin/cdn/rdforms/samples/bootstrap/templates/adms.json'],
  ['/engin/cdn/rdforms/samples/bootstrap/templates/vcard.json'],
  ['/engin/cdn/rdforms/samples/bootstrap/templates/dcat_props.json'],
  ['/engin/cdn/rdforms/samples/bootstrap/templates/dcat.json'],
  ['/engin/cdn/rdforms/samples/bootstrap/templates/templateBundle.json'],
];

registeryDummyChooser();

const { Graph } = rdfjson;
const { ItemStore, bundleLoader, Editor } = rdforms;
const itemStore = new ItemStore();
const graph = new Graph(rdfGraph);
bundleLoader(itemStore, bundles, () => {
  new Editor({
    graph,
    resource: 'http://example.org/about',
    template: itemStore.getItem('test'),
    compact: false,
    includeLevel: 'optional',
  }, 'node');
  const ta = document.getElementById('output');
  const updateOutput = () => {
    // Export RDF/XML
    ta.value = rdfjson.converters.rdfjson2rdfxml(graph);

    // Export RDF/JSON
    // ta.value = JSON.stringify(graph.exportRDFJSON(), null, "  ");

    setTimeout(function () {
        $('.fas').addClass('fa');
    }, 1000);
  };

  var hs = document.getElementsByTagName('style'),i=6;
  hs[i].parentNode.removeChild(hs[i])

  updateOutput();
  graph.onChange = updateOutput;
});

$(function () {
    /*
    Endpoint.ajax('tree', "", function (data) {
        $('#forest').html(data);

        $('#forest').treed({openedClass:'fa-minus', closedClass:'fa-plus'});

        $('#forest span.directory').click(function (ev) {
            var path = $(ev.currentTarget).attr('data-path');

            console.log(path);

            navigate(path);
        });

        navigate('.');
    },null,'html');
    //*/

    // create the editor
    const container = document.getElementById("schema");
    const options = {};
    const editor = new JSONEditor(container, options);

    // set json
    const initialJson = {
        "Array": [1, 2, 3],
        "Boolean": true,
        "Null": null,
        "Number": 123,
        "Object": {"a": "b", "c": "d"},
        "String": "Hello World"
    }
    editor.set(initialJson);

    const updatedJson = editor.get();
});

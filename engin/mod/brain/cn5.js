var Concept7 = {
    sql: function (tx,query,handler) {
        tx.executeSql(query, [], handler);
    },
    cfg: {
        uuid: 'concept7',
        numb: '1.0',
        text: "ConceptNet-like mechanism for Natural Language",
        size: 128 * 1024 * 1024,
    },
    ini: function () {
        Concept7.dbo = openDatabase(Concept7.cfg.uuid, Concept7.cfg.numb, Concept7.cfg.text, Concept7.cfg.size);
    },
    dbo: null,
};

Concept7.ini();

Concept7.dbo.transaction(function (tx) {
    var data = [
        ['en', "english", "English"],
        ['fr', "french",  "Français"],
        ['es', "spanish", "Español"],
        ['de', "german",  "Deutsch"],
        ['ru', "russian", "русский"],
        ['zh', "chinese", "中文"],

        ['it', "italian",    "Italiano"],
        ['nl', "dutch",      "Nederlands"],
        ['pt', "portuguese", "Português"],
        ['ja', "japanese",   "日本語"],
    ];

    tx.executeSql('CREATE TABLE IF NOT EXISTS language (code,flag,name,text)');

    for (i=0 ; i<data.length ; i++) {
        tx.executeSql('INSERT INTO language (code,flag,name,text) VALUES ("'+data[i][0]+'","'+data[i][1]+'","'+data[i][0]+'","'+data[i][2]+'")');
    }

    Concept7.sql(tx,'SELECT * FROM language', function (tx, results) {
        console.log("SQL results : ", results);
    });

    tx.executeSql('CREATE TABLE IF NOT EXISTS grammar (lang,text,name,kind,when,numb)');
});
/*
Concept7.sql('SELECT * FROM language', function (tx, results) {
    var len = results.rows.length, i;

    msg = "<p>Found rows: " + len + "</p>";

    document.querySelector('#status').innerHTML +=  msg;

    for (i = 0; i < len; i++) {
      alert(results.rows.item(i).log );
    }

    console.log("SQL results : ", results);
});
//*/
$(function () {
    $.ajax({
        success: function (data) {
            data = data.split('\n');

            Concept7.dbo.transaction(function (tx) {
                for (var i=0 ; i<data.length ; i++) {
                    tx.executeSql('INSERT INTO verbs (lang,text,name,kind,when,numb) VALUES ("en","'+data[i]+'","?","?","?","?")');
                }

                Concept7.sql(tx, 'SELECT * FROM verbs', function (ty, results) {
                    console.log("SQL results : ", results);
                });
            });
        },
        dataType: 'html',
        url: "/brain/txt/english/verbs/31K.txt",
    });
    /*
    Endpoint.ajax('tree', "", function (data) {
        $('#forest').html(data);

        $('#forest').treed({openedClass:'fa-minus', closedClass:'fa-plus'});

        $('#forest span.directory').click(function (ev) {
            var path = $(ev.currentTarget).attr('data-path');

            console.log(path);

            navigate(path);
        });

        navigate('.');
    },null,'html');
    //*/
});
/*
function addItem(){
	var ul = document.getElementById("dynamic-list");
  var candidate = document.getElementById("candidate");
  var li = document.createElement("li");
  li.setAttribute('id',candidate.value);
  li.appendChild(document.createTextNode(candidate.value));
  ul.appendChild(li);
}

function removeItem(){
	var ul = document.getElementById("dynamic-list");
  var candidate = document.getElementById("candidate");
  var item = document.getElementById(candidate.value);
  ul.removeChild(item);
}
<ul id="dynamic-list"></ul>
<input type="text" id="candidate"/>
<button onclick="addItem()">add item</button>
<button onclick="removeItem()">remove item</button>
//*/

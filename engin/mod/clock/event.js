function event_cosmic (value,result) {
    var solar = SunCalc.getTimes(value, 51.5, -0.1);
    var lunar = SunCalc.getMoonTimes(value, 51.5, -0.1);
    var phase = SunCalc.getMoonIllumination(value);
    
    result.push({
        "title": 'Sun Rise',
        "start": solar.sunrise.toISOString(),
        "end": solar.sunriseEnd.toISOString(),
    });
    result.push({
        "title": 'Golden Hour',
        "start": solar.goldenHour.toISOString(),
        "end": solar.goldenHourEnd.toISOString(),
    });
    result.push({
        "title": 'Sun Set',
        "start": solar.sunsetStart.toISOString(),
        "end": solar.sunset.toISOString(),
    });

    result.push({
        "title": 'Civil Day',
        "start": solar.dawn.toISOString(),
        "end": solar.dusk.toISOString(),
    });
    result.push({
        "title": 'Nautical Day',
        "start": solar.nauticalDawn.toISOString(),
        "end": solar.nauticalDusk.toISOString(),
    });

    result.push({
        "title": 'Solar Noon',
        "start": solar.solarNoon.toISOString(),
    });

    result.push({
        "title": 'Night',
        "start": solar.night.toISOString(),
        "end": solar.nightEnd.toISOString(),
    });

    result.push({
        "title": 'Moon Light',
        "start": lunar.rise.toISOString(),
        "end": lunar.set.toISOString(),
    });

    /*
    $('#moon span.sight_up').html((lunar.alwaysUp)?'true':'false');
    $('#moon span.sight_down').html((lunar.alwaysDown)?'true':'false');

    // get position of the sun (azimuth and altitude) at today's sunrise
    var sunPos = SunCalc.getPosition(solar.sunrise, 51.5, -0.1);

    //$('#sun span.sunpos').html(sunPos);

    // get sunrise azimuth in degrees
    $('#sun span.azimuth').html(sunPos.azimuth * 180 / Math.PI);
    //*/

    return result;
}

function all_events (value) {
    var result = [];

    result = event_cosmic(value,result);

    return result;
}

  var today = new Date();

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('agenda');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      height: '100%',
      expandRows: true,
      slotMinTime: '08:00',
      slotMaxTime: '20:00',
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
      },
      initialView: 'listWeek',
      initialDate: today,
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      selectable: true,
      nowIndicator: true,
      dayMaxEvents: true, // allow "more" link when too many events
      events: all_events(today),
    });

    calendar.render();
  });


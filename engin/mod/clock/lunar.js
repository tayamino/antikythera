function getMoonPhase(year, month, day)
{
    var c = e = jd = b = 0;

    if (month < 3) {
        year--;
        month += 12;
    }

    ++month;

    c = 365.25 * year;

    e = 30.6 * month;

    jd = c + e + day - 694039.09; //jd is total days elapsed

    jd /= 29.5305882; //divide by the moon cycle

    b = parseInt(jd); //int(jd) -> b, take integer part of jd

    jd -= b; //subtract integer part to leave fractional part of original jd

    b = Math.round(jd * 8); //scale fraction from 0-8 and round

    if (b >= 8 ) {
        b = 0; //0 and 8 are the same so turn 8 into 0
    }

    // 0 => New Moon
    // 1 => Waxing Crescent Moon
    // 2 => Quarter Moon
    // 3 => Waxing Gibbous Moon
    // 4 => Full Moon
    // 5 => Waning Gibbous Moon
    // 6 => Last Quarter Moon
    // 7 => Waning Crescent Moon
    
    return b;
}

function Moondraws( canvas ) {
	this.lineWidth = 10;
	this.radius = canvas.width / 2 - this.lineWidth / 2;
	this.offset = this.lineWidth / 2;

	this.canvas = canvas;
	this.ctx = canvas.getContext( '2d' );
}

Moondraws.prototype = {
	_drawDisc: function() {
		this.ctx.translate( this.offset, this.offset ) ;
		this.ctx.beginPath();
		this.ctx.arc( this.radius, this.radius, this.radius, 0, 2 * Math.PI, true );
		this.ctx.closePath();
		this.ctx.fillStyle = '#fff';
		this.ctx.strokeStyle = '#fff';
		this.ctx.lineWidth = this.lineWidth;

		this.ctx.fill();			
		this.ctx.stroke();
	},

	_drawPhase: function( phase ) {
		this.ctx.beginPath();
		this.ctx.arc( this.radius, this.radius, this.radius, -Math.PI/2, Math.PI/2, true );
		this.ctx.closePath();
		this.ctx.fillStyle = '#000';
		this.ctx.fill();

		this.ctx.translate( this.radius, this.radius );
		this.ctx.scale( phase, 1 );
		this.ctx.translate( -this.radius, -this.radius );
		this.ctx.beginPath();
		this.ctx.arc( this.radius, this.radius, this.radius, -Math.PI/2, Math.PI/2, true );
		this.ctx.closePath();
		this.ctx.fillStyle = phase > 0 ? '#fff' : '#000';
		this.ctx.fill();
	},
	
	/**
	 * @param {Number} The phase expressed as a float in [0,1] range .
	 */	
	paint( phase ) {
		this.ctx.save();
		this.ctx.clearRect( 0, 0, this.canvas.width, this.canvas.height );

		if ( phase <= 0.5 ) {
			this._drawDisc();
			this._drawPhase( 4 * phase - 1 );
		} else {
			this.ctx.translate( this.radius + 2 * this.offset, this.radius + 2 * this.offset );
			this.ctx.rotate( Math.PI );
			this.ctx.translate( -this.radius, -this.radius );

			this._drawDisc();
			this._drawPhase( 4 * ( 1 - phase ) - 1 );
		}

		this.ctx.restore();		
	}
}

var moons = [
    'New Moon',
    'Waxing Crescent',
    'First Quarter',
    'Waxing Gibbous',
    'Full Moon',
    'Waning Gibbous',
    'Last Quarter',
    'Waning Crescent',
];
var draws = null;
var phase = 0;
var today = new Date();

function repaint() {
    phase += 0.02;
	
	draws.paint( phase );
	
	if ( phase > 1 ) {
		phase = 0;
	}

	setTimeout( repaint, 50 );
}

function times_value(orbit,alias,field,value) {
    // format sunrise time from the Date object
    $('#'+orbit+' span.'+alias+'_'+field).html(value.getHours() + ':' + value.getMinutes());
}
function times_range(orbit,alias,since,until) {
    times_value(orbit,alias,'since',since);
    times_value(orbit,alias,'until',until);
}

jQuery(function ($) {
    // get today's sunlight times for London
    var today = new Date();
    var solar = SunCalc.getTimes(today, 51.5, -0.1);
    var lunar = SunCalc.getMoonTimes(today, 51.5, -0.1);
    var illum = SunCalc.getMoonIllumination(today);
    var phase = getMoonPhase(today.getFullYear(),today.getMonth(),today.getDay());
    
    times_range('sun', 'sunrise',    solar.sunrise,     solar.sunriseEnd);
    times_range('sun', 'goldenhour', solar.goldenHour,  solar.goldenHourEnd);
    times_range('sun', 'sunset',     solar.sunsetStart, solar.sunset);

    times_value('day', 'dawn', 'civil', solar.dawn);
    times_value('day', 'dusk', 'civil', solar.dusk);

    times_value('day', 'dawn', 'ocean', solar.nauticalDawn);
    times_value('day', 'dusk', 'ocean', solar.nauticalDusk);

    times_value('day', 'noon', 'solar', solar.solarNoon);

    times_range('moon', 'night', solar.night, solar.nightEnd);

    //times_range('moon', 'light', lunar.rise, lunar.set);

    $('#moon span.sight_up').html((lunar.alwaysUp)?'true':'false');
    $('#moon span.sight_down').html((lunar.alwaysDown)?'true':'false');

    // get position of the sun (azimuth and altitude) at today's sunrise
    var sunPos = SunCalc.getPosition(solar.sunrise, 51.5, -0.1);

    //$('#sun span.sunpos').html(sunPos)

    // get sunrise azimuth in degrees
    $('#sun span.azimuth').html(sunPos.azimuth * 180 / Math.PI);

    draws = new Moondraws( document.getElementById( 'canvas' ) );

    //repaint();

    $('#title').html(moons[phase]);

	draws.paint( phase / 8.0 );
});


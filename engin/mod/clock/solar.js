function times_value(orbit,alias,field,value) {
    // format sunrise time from the Date object
    $('#'+orbit+' span.'+alias+'_'+field).html(value.getHours() + ':' + value.getMinutes());
}
function times_range(orbit,alias,since,until) {
    times_value(orbit,alias,'since',since);
    times_value(orbit,alias,'until',until);
}

jQuery(function ($) {
    // get today's sunlight times for London
    var today = new Date();
    var solar = SunCalc.getTimes(today, 51.5, -0.1);
    var lunar = SunCalc.getMoonTimes(today, 51.5, -0.1);
    var phase = SunCalc.getMoonIllumination(today);
    
    times_range('sun', 'sunrise',    solar.sunrise,     solar.sunriseEnd);
    times_range('sun', 'goldenhour', solar.goldenHour,  solar.goldenHourEnd);
    times_range('sun', 'sunset',     solar.sunsetStart, solar.sunset);

    times_value('day', 'dawn', 'civil', solar.dawn);
    times_value('day', 'dusk', 'civil', solar.dusk);

    times_value('day', 'dawn', 'ocean', solar.nauticalDawn);
    times_value('day', 'dusk', 'ocean', solar.nauticalDusk);

    times_value('day', 'noon', 'solar', solar.solarNoon);

    times_range('moon', 'night', solar.night, solar.nightEnd);

    times_range('moon', 'light', lunar.rise, lunar.set);

    $('#moon span.sight_up').html((lunar.alwaysUp)?'true':'false');
    $('#moon span.sight_down').html((lunar.alwaysDown)?'true':'false');

    // get position of the sun (azimuth and altitude) at today's sunrise
    var sunPos = SunCalc.getPosition(solar.sunrise, 51.5, -0.1);

    //$('#sun span.sunpos').html(sunPos)

    // get sunrise azimuth in degrees
    $('#sun span.azimuth').html(sunPos.azimuth * 180 / Math.PI);
});

let size = 14;

function refresh () {
    const element = document.querySelector('.universe')
    element.style.fontSize = `${size}px`
}

(() => {
	document.querySelector('.universe').addEventListener('wheel', (event) => {
		const MaxSize = 32;
		const MinSize = 8;
		
		const deltaY = event.deltaY
		
		if (deltaY < 0) {
			size++
			if (size > MaxSize) size = MaxSize
		}
		
		if (deltaY > 0) {
			size--
			if (size < MinSize) size = MinSize
		}

        refresh();
	})
    refresh();
})();


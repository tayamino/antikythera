ChronoSphere = function (alias,title,state) {
    this.alias = alias;
    this.title = title;
    this.state = state;

    this.index = undefined;

    this.domEl = $('#'+alias+'.chrono');

    this.read();
    this.save();

    $('#'+alias+'.chrono button[name="start"]').click(function (ev) {
        var k = $(ev.currentTarget).parent('.chrono').attr('id');

        KyroS[k].start();
    });
    $('#'+alias+'.chrono button[name="stop"]').click(function (ev) {
        var k = $(ev.currentTarget).parent('.chrono').attr('id');

        KyroS[k].stop();
    });
    $('#'+alias+'.chrono h4').html(title);
}

var KyroS = {};

ChronoSphere.prototype.read = function () {
    this.value = (localStorage[this.named()] || 0.0);
}
ChronoSphere.prototype.save = function () {
    localStorage[this.named()] = this.value;

    this.domEl.children('p').html((+this.value).toFixed(1));
}
ChronoSphere.prototype.named = function () { return 'chrono_'+this.alias; }

ChronoSphere.prototype.reset = function () {
    window.clearInterval(this.index);
    this.index = undefined;
    localStorage[this.named()] = 0.0;
    this.start();
}
ChronoSphere.prototype.stop = function () {
    // Stop chrono
    if (this.index) {
        window.clearInterval(i);
        //changeToStart();
        this.index = undefined;
    }
    // Start chrono
    else {
        //changeToStop();
        this.start();
    }
}
ChronoSphere.prototype.loop = function () {
    this.value = +this.value + 0.1;

    this.save();
}
ChronoSphere.prototype.start = function () {
    this.save();

    this.index = setInterval(this.loop, 100);
}
// Start chrono and update each 0.1 seconds

tm1 = new ChronoSphere('sphere1', "Sphere 1",false);
tm2 = new ChronoSphere('sphere2', "Sphere 2",false);

KyroS[tm1.alias] = tm1;
KyroS[tm2.alias] = tm2;


function graphQLendpoint() {
    //return 'https://swapi-graphql.netlify.com/.netlify/functions/index';

    var name = window.location.search.replace('?name=',''); // Endpoint.args.name;

    var path ; //= Endpoint.conn.link;

    switch (name) {
      case 'brain':
          path = "/hubot/ql";
          break;
      case 'nosql':
          path = "/model/api";
          break;
      case 'sqldb':
          path = "/daten/api";
          break;
      case 'proxy':
          path = "/graph/api";
          break;
      case 'crawl':
          path = "/graph/web";
          break;
      default:
          path = "/graph/ql";
    }

    link = 'http://localhost:4747' + path;

    $('#endpointLink').html('<a href="'+link+'" target="_new">'+path+'</a>');

    return link;

    return Endpoint.conn.link + resp;
}

function graphQLFetcher(graphQLParams) {
return fetch(
  graphQLendpoint(),
  {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(graphQLParams),
    credentials: 'omit',
  },
).then(function (response) {
  return response.json().catch(function () {
    return response.text();
  });
});
}

ReactDOM.render(
    React.createElement(GraphiQL, {
      fetcher: graphQLFetcher,
      defaultVariableEditorOpen: true,
    }),
    document.getElementById('graphiql'),
);

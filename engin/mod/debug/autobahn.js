function logging (...args) {
    if (false) {
        console.log(...args);
    } else {
        var resp = "<p>";

        for (var i=0 ; i<args.length ; i++) {
            resp += args[i] + " ";
        }

        resp += "</p>";

        $('#logging').append(resp);
    }
}

 var wsuri = (document.location.protocol === "http:" ? "ws:" : "wss:") + "//" + document.location.host + "/wsock";

 var connection = new autobahn.Connection({
    url:   JSON.parse(localStorage['ep_autobahn_link']),
    realm: JSON.parse(localStorage['ep_autobahn_name']),
 });

 var session = null;

 //
 // Meta Procedures for Sessions
 //

 // wamp.session.kill
 function kill_session () {
    if (session) {
       var session_id = parseInt(document.getElementById('kill_session_session_id').value);
       var reason = document.getElementById('kill_session_reason').value;
       var message = document.getElementById('kill_session_message').value;

       session.call("wamp.session.kill", [session_id, reason, message]).then(
          function () {
             logging("session " + session_id + " killed!");
          },
          function (err) {
             logging("could not kill session " + session_id, err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.session.kill_by_authid
 function kill_session_authid () {
    if (session) {
       var authid = document.getElementById('kill_session2_session_authid').value;
       var reason = document.getElementById('kill_session2_reason').value;
       var message = document.getElementById('kill_session2_message').value;

       session.call("wamp.session.kill_by_authid", [authid, reason, message]).then(
          function (res) {
             logging("sessions killed: " + res);
          },
          function (err) {
             logging("could not kill sessions (authid='" + authid + "')", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.session.kill_by_authrole
 function kill_session_authrole () {
    if (session) {
       var authrole = document.getElementById('kill_session3_session_authrole').value;
       var reason = document.getElementById('kill_session3_reason').value;
       var message = document.getElementById('kill_session3_message').value;

       session.call("wamp.session.kill_by_authrole", [authrole, reason, message]).then(
          function (res) {
             logging("sessions killed: " + res);
          },
          function (err) {
             logging("could not kill sessions (authrole='" + authrole + "')", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.session.list
 // wamp.session.get
 function list_sessions () {
    if (session) {
       session.call("wamp.session.list").then(
          function (sessions) {
             logging("Current session IDs on realm", sessions);
             for (var i = 0; i < sessions.length; ++i) {
                session.call("wamp.session.get", [sessions[i]]).then(
                   function (session_details) {
                      logging(session_details);
                   },
                   function (err) {
                      logging(err);
                   }
                );
             }
          },
          function (err) {
             logging("Could not retrieve subscription for topic", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.session.count
 function count_sessions () {
    if (session) {
       session.call("wamp.session.count").then(
          function (session_count) {
             logging("Current number of sessions joined on the realm: " + session_count);
          },
          function (err) {
             logging("Could not retrieve number of sessions", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 //
 // Meta Procedures for Registrations
 //

 // procedure1
 //
 var procedure1_uri = "com.example.procedure1";
 var procedure1_options = {};

 function procedure1_fill_options () {
    procedure1_uri = document.getElementById('registration_uri').value;
    procedure1_options = {};

    if (document.getElementById('registration_match_default').checked) {
    }
    else if (document.getElementById('registration_match_exact').checked) {
       procedure1_options.match = 'exact';
    }
    else if (document.getElementById('registration_match_prefix').checked) {
       procedure1_options.match = 'prefix';
    }
    else if (document.getElementById('registration_match_wildcard').checked) {
       procedure1_options.match = 'wildcard';
    }
 }

 // wamp.registration.lookup
 // wamp.registration.get
 function procedure1_get_registration () {
    if (session) {
       procedure1_fill_options();
       session.call("wamp.registration.lookup", [procedure1_uri, procedure1_options]).then(
          function (registration_id) {
             if (registration_id) {
                session.call("wamp.registration.get", [registration_id]).then(
                   function (registration) {
                      logging("Procedure " + procedure1_uri + " is managed via registration", registration);
                   },
                   function (err) {
                      logging("Could not retrieve registration", err);
                   }
                );
             } else {
                logging("No registration for procedure " + procedure1_uri + " currently exists on dealer");
             }
          },
          function (err) {
             logging("Could not retrieve registration for procedure", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.registration.lookup
 // wamp.registration.list_callees
 function procedure1_list_callees () {
    if (session) {
       procedure1_fill_options();
       session.call("wamp.registration.lookup", [procedure1_uri, procedure1_options]).then(
          function (registration_id) {
             if (registration_id) {
                session.call("wamp.registration.list_callees", [registration_id]).then(
                   function (callees) {
                      logging("Callees on procedure " + procedure1_uri + " (registration " + registration_id + "):", callees);
                   },
                   function (err) {
                      logging("Could not retrieve calless on registration", err);
                   }
                );
             } else {
                logging("No registration for procedure " + procedure1_uri + " currently exists on dealer");
             }
          },
          function (err) {
             logging("Could not retrieve registration for procedure", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.registration.lookup
 // wamp.registration.count_callees
 function procedure1_count_callees () {
    if (session) {
       procedure1_fill_options();
       session.call("wamp.registration.lookup", [procedure1_uri, procedure1_options]).then(
          function (registration_id) {
             if (registration_id) {
                session.call("wamp.registration.count_callees", [registration_id]).then(
                   function (count) {
                      if (count) {
                         logging("" + count + " callees on procedure " + procedure1_uri);
                      } else {
                         logging("No callees on procedure " + procedure1_uri);
                      }
                   },
                   function (err) {
                      logging("Could not retrieve callee count on registration", err);
                   }
                );
             } else {
                logging("No registration for procedure " + procedure1_uri + " currently exists on dealer");
             }
          },
          function (err) {
             logging("Could not retrieve registration for procedure", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.registration.list
 // wamp.registration.get
 function get_registrations () {
    if (session) {
       session.call("wamp.registration.list").then(
          function (registrations) {
             logging("Current registrations", registrations);
             var match_policies = ['exact', 'prefix', 'wildcard'];

             // for each match policy ..
             for (var j = 0; j < match_policies.length; ++j) {
                (function (l) {
                   var match_policy = match_policies[l];

                   // .. get the registrations
                   for (var i = 0; i < registrations[match_policy].length; ++i) {
                      (function (k) {
                         session.call("wamp.registration.get", [registrations[match_policy][k]]).then(
                            function (registration) {
                               logging("Registration", registration);
                            },
                            function (err) {
                               logging(err);
                            }
                         );
                      })(i);
                   }
                })(j);
             }
          },
          function (err) {
             logging("Could not retrieve registrations", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.registration.remove_callee
 function remove_callee () {

    var registration_id = parseInt(document.getElementById('remove_callee_registration_id').value);
    var callee_id = parseInt(document.getElementById('remove_callee_callee_id').value);
    var reason = document.getElementById('remove_callee_reason').value;

    if (session) {
       session.call("wamp.registration.remove_callee", [registration_id, callee_id, reason]).then(
          function (res) {
             logging("callee removed from registration: ", res);
          },
          function (err) {
             logging(err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 //
 // Meta Procedures for Subscriptions
 //

 // topic1
 //
 var topic1_uri = "com.example.topic1";
 var topic1_options = {};

 function topic1_fill_options () {
    topic1_uri = document.getElementById('subscription_uri').value;
    topic1_options = {};

    if (document.getElementById('subscription_match_default').checked) {
    }
    else if (document.getElementById('subscription_match_exact').checked) {
       topic1_options.match = 'exact';
    }
    else if (document.getElementById('subscription_match_prefix').checked) {
       topic1_options.match = 'prefix';
    }
    else if (document.getElementById('subscription_match_wildcard').checked) {
       topic1_options.match = 'wildcard';
    }
 }

 // wamp.subscription.lookup
 // wamp.subscription.get
 function topic1_get_subscription () {
    if (session) {
       topic1_fill_options();
       session.call("wamp.subscription.lookup", [topic1_uri, topic1_options]).then(
          function (subscription_id) {
             if (subscription_id) {
                session.call("wamp.subscription.get", [subscription_id]).then(
                   function (subscription) {
                      logging("Topic " + topic1_uri + " is managed via subscription", subscription);
                   },
                   function (err) {
                      logging("Could not retrieve subscription", err);
                   }
                );
             } else {
                logging("No subscription for topic " + topic1_uri + " currently exists on broker");
             }
          },
          function (err) {
             logging("Could not retrieve subscription for topic", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.subscription.lookup
 // wamp.subscription.list_subscribers
 function topic1_list_subscribers () {
    if (session) {
       topic1_fill_options();
       session.call("wamp.subscription.lookup", [topic1_uri, topic1_options]).then(
          function (subscription_id) {
             if (subscription_id) {
                session.call("wamp.subscription.list_subscribers", [subscription_id]).then(
                   function (subscribers) {
                      logging("Subscribers on topic " + topic1_uri + " (subscription " + subscription_id + "):", subscribers);
                   },
                   function (err) {
                      logging("Could not retrieve subscribers on subscription", err);
                   }
                );
             } else {
                logging("No subscription for topic " + topic1_uri + " currently exists on broker");
             }
          },
          function (err) {
             logging("Could not retrieve subscription for topic", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.subscription.lookup
 // wamp.subscription.count_subscribers
 function topic1_count_subscribers () {
    if (session) {
       topic1_fill_options();
       session.call("wamp.subscription.lookup", [topic1_uri, topic1_options]).then(
          function (subscription_id) {
             if (subscription_id) {
                session.call("wamp.subscription.count_subscribers", [subscription_id]).then(
                   function (count) {
                      if (count) {
                         logging("" + count + " subscribers on topic " + topic1_uri);
                      } else {
                         logging("No subscribers on topic " + topic1_uri);
                      }
                   },
                   function (err) {
                      logging("Could not retrieve subscriber count on subscription", err);
                   }
                );
             } else {
                logging("No subscription for topic " + topic1_uri + " currently exists on broker");
             }
          },
          function (err) {
             logging("Could not retrieve subscription for topic", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.subscription.match
 // wamp.subscription.list_subscribers
 function match_subscriptions () {
    if (session) {

       var match_subscriptions_uri = document.getElementById('match_subscriptions_uri').value;

       session.call("wamp.subscription.match", [match_subscriptions_uri]).then(
          function (subscription_ids) {
             logging(subscription_ids);
          },
          function (err) {
             logging("Could not retrieve subscription for topic", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.subscription.list
 // wamp.subscription.get
 function get_subscriptions () {
    if (session) {
       session.call("wamp.subscription.list").then(
          function (subscriptions) {
             logging("Current subscriptions:", subscriptions);
             var match_policies = ['exact', 'prefix', 'wildcard'];

             // for each match policy ..
             for (var j = 0; j < match_policies.length; ++j) {
                (function (l) {
                   var match_policy = match_policies[l];

                   // .. get the registrations
                   for (var i = 0; i < subscriptions[match_policy].length; ++i) {
                      (function (k) {
                         session.call("wamp.subscription.get", [subscriptions[match_policy][k]]).then(
                            function (subscription) {
                               logging("Subscription", subscription);
                            },
                            function (err) {
                               logging(err);
                            }
                         );
                      })(i);
                   }
                })(j);
             }
          },
          function (err) {
             logging("Could not retrieve subscriptions", err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 // wamp.subscription.remove_subscriber
 function remove_subscriber () {

    var subscription_id = parseInt(document.getElementById('remove_subscriber_subscription_id').value);
    var subscriber_id = parseInt(document.getElementById('remove_subscriber_subscriber_id').value);
    var reason = document.getElementById('remove_subscriber_reason').value;

    if (session) {
       session.call("wamp.subscription.remove_subscriber", [subscription_id, subscriber_id, reason]).then(
          function (res) {
             logging("subscriber removed from subscription: ", res);
          },
          function (err) {
             logging(err);
          }
       );
    } else {
       logging("not connected");
    }
 }

 function close_connection () {
    if (session) {
       session.leave();
    } else {
       logging("not connected");
    }
 }

 //
 function refresh_ui () {
   if (session) {
      session.call("wamp.session.list").then(
         function (sessions) {
            logging("Current session IDs on realm", sessions);
            for (var i = 0; i < sessions.length; ++i) {
               session.call("wamp.session.get", [sessions[i]]).then(
                  function (session_details) {
                     $('#session ul').append('<li>'+session_details.session+'</li>');
                  },
                  function (err) {
                     logging(err);
                  }
               );
            }
         },
         function (err) {
            logging("Could not retrieve subscription for topic", err);
         }
      );
      session.call("wamp.registration.list").then(
         function (registrations) {
            logging("Current registrations", registrations);
            var match_policies = ['exact', 'prefix', 'wildcard'];

            // for each match policy ..
            for (var j = 0; j < match_policies.length; ++j) {
               (function (l) {
                  var match_policy = match_policies[l];

                  // .. get the registrations
                  for (var i = 0; i < registrations[match_policy].length; ++i) {
                     (function (k) {
                        session.call("wamp.registration.get", [registrations[match_policy][k]]).then(
                           function (registration) {
                               $('#method ul').append('<li>'+registration+'</li>');
                               console.log("Registration", registration);
                           },
                           function (err) {
                              logging(err);
                           }
                        );
                     })(i);
                  }
               })(j);
            }
         },
         function (err) {
            logging("Could not retrieve registrations", JSON.stringify(err));
         }
      );
      session.call("wamp.subscription.list").then(
         function (subscriptions) {
            logging("Current subscriptions:", subscriptions);
            var match_policies = ['exact', 'prefix', 'wildcard'];

            // for each match policy ..
            for (var j = 0; j < match_policies.length; ++j) {
               (function (l) {
                  var match_policy = match_policies[l];

                  // .. get the registrations
                  for (var i = 0; i < subscriptions[match_policy].length; ++i) {
                     (function (k) {
                        session.call("wamp.subscription.get", [subscriptions[match_policy][k]]).then(
                           function (subscription) {
                               $('#method ul').append('<li>'+subscription+'</li>');
                               console.log("Subscription", subscription);
                           },
                           function (err) {
                              logging(err);
                           }
                        );
                     })(i);
                  }
               })(j);
            }
         },
         function (err) {
            logging("Could not retrieve subscriptions", JSON.stringify(err));
         }
      );
   } else {
      logging("not connected");
   }
 }
 //
 connection.onopen = function (new_session, details) {

    session = new_session;

    logging("Connected with session ID " + session.id, details);

    var meta_topics = [
       // Meta Events for Sessions
       "wamp.session.on_join",
       "wamp.session.on_leave",

       // Meta Events for Subscriptions
       "wamp.subscription.on_create",
       "wamp.subscription.on_subscribe",
       "wamp.subscription.on_unsubscribe",
       "wamp.subscription.on_delete",

       // Meta Events for Registrations
       "wamp.registration.on_create",
       "wamp.registration.on_register",
       "wamp.registration.on_unregister",
       "wamp.registration.on_delete",

       // Meta Events for Schemas
       "wamp.schema.on_define",
       "wamp.schema.on_undefine",
    ];

    // subscribe to all of above WAMP meta events
    for (var i = 0; i < meta_topics.length; ++i) {

       (function subscribe (topic) {

          function on_meta_event (args, kwargs) {
             logging("WAMP meta event " + topic + " received", args, kwargs);
          }

          session.subscribe(topic, on_meta_event).then(
             function (sub) {
                logging('Subscribed to meta topic ' + topic);
             },
             function (err) {
                logging('Failed to subscribe to meta topic ' + topic, err);
             }
          );

       })(meta_topics[i]);
    }

    refresh_ui();
 };

 connection.onclose = function (reason, details) {
    logging("Connection lost: " + reason);
 }

 connection.open();

 function open_connection () {
     connection.open();
 }

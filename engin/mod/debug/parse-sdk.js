var ParseSDK = {
    type: {

    },
    func: {
    },
    user: {
        list: function (callback) {

        },
    },
    init: function (options) {
        if (ParseSDK.conf==null) {
            ParseSDK.conf = {
              target: localStorage['ep_parse_target'],
              app_id: localStorage['ep_parse_app_id'],
              master: localStorage['ep_parse_master'],
              rest_x: localStorage['ep_parse_rest_x'],
            };
            /*
            try {
                ParseSDK.conf = JSON.parse(localStorage['parse_conf']);
            } catch (ex) {
                ParseSDK.conf = {
                    target: "http://localhost:4747/model/rpc",
                    app_id: 'gestalt',
                    master: "0f84ffdc-feaf-45c5-a87a-2150a40b71fc",
                    rest_x: "0f84ffdc-feaf-45c5-a87a-2150a40b71fc",
                };
            }

            localStorage['parse_conf'] = JSON.stringify(ParseSDK.conf);

            $('#parse_target').val(ParseSDK.conf.target);
            $('#parse_app_id').val(ParseSDK.conf.app_id);
            $('#parse_master').val(ParseSDK.conf.master);
            $('#parse_rest_x').val(ParseSDK.conf.rest_x);
            //*/
        }
    },
    call: function (verb,link,data,hook) {
        $.ajax({
            method:  verb,
            url:     ParseSDK.conf.target+"/"+link,
            data:    data,
            headers: {
                'X-Parse-Application-Id': ParseSDK.conf.app_id,
                'X-Parse-Master-Key':     ParseSDK.conf.master,
                //'X-Parse-REST-API-Key':   ParseSDK.conf.rest_x,
            },
            success: hook,
        });
    },
    save: function () {
        ParseSDK.conf.target = $('#parse_target').val();
        ParseSDK.conf.app_id = $('#parse_app_id').val();
        ParseSDK.conf.master = $('#parse_master').val();
        ParseSDK.conf.rest_x = $('#parse_rest_x').val();

        localStorage['parse_conf'] = JSON.stringify(ParseSDK.conf);
    },
    conf: null,
};

ParseSDK.init();

/*
curl -X GET \
  -H "X-Parse-Application-Id: ${APPLICATION_ID}" \
  -H "X-Parse-REST-API-Key: ${REST_API_KEY}" \
  https://YOUR.PARSE-SERVER.HERE/parse/users
//*/

function refresh_table (ev) {
  var name = ev.currentTarget.innerHTML;

  ParseSDK.call('GET','schemas/'+name,null,function (meta) {
    var col = [
      {title:"ID", field:"objectId", editor:"input"},
      /*
      {title:"Task Progress", field:"progress", hozAlign:"left", formatter:"progress", editor:true},
      {title:"Gender", field:"gender", width:95, editor:"select", editorParams:{values:["male", "female"]}},
      {title:"Rating", field:"rating", formatter:"star", hozAlign:"center", width:100, editor:true},
      {title:"Color", field:"col", width:130, editor:"input"},
      {title:"Date Of Birth", field:"dob", width:130, sorter:"date", hozAlign:"center"},
      {title:"Driver", field:"car", width:90,  hozAlign:"center", formatter:"tickCross", sorter:"boolean", editor:true},
      //*/
    ];

    $.each(meta.fields, function (k,v) {
      switch (k) {
        case 'objectId':
        case 'ACL':
        case 'createdAt':
        case 'updatedAt':
          break;
        default:
          col.push({title:k, field:k, editor:"input"})
      }
    })

    ParseSDK.call('GET','classes/'+name,null,function (data) {
      var table = new Tabulator("#collection", {
          data:data.results,           //load row data from array
          layout:"fitColumns",      //fit columns to width of table
          responsiveLayout:"hide",  //hide columns that dont fit on the table
          addRowPos:"top",          //when adding a new row, add it to the top of the table
          tooltips:true,            //show tool tips on cells
          history:true,             //allow undo and redo actions on the table
          movableColumns:true,      //allow column order to be changed
          resizableRows:true,       //allow row order to be changed
          pagination:"local",       //paginate the data
          paginationSize:7,         //allow 7 rows per page of data
          //*
          initialSort:[
              {column:"objectId", dir:"asc"},
          ],
          //*/
          columns:col,
      });
    });

    $('ul#parse_type li').click(refresh_table);
  });

  console.log("ParseSDK type : ",name);
}
function delete_table (ev) {
  var name = ev.currentTarget.previousSibling.innerHTML;

  ParseSDK.call('DELETE','schemas/'+name,null,function (data) {
    ev.currentTarget.parentNode.remove();
    console.log("Delete Schema : ",name);
  });
}

jQuery(function ($) {
    ParseSDK.call('GET','schemas',null,function (data) {
      var item;

      for (var i=0 ; i<data.results.length ; i++) {
        item = data.results[i];

        if (item.className[0]!='_') {
          $('#parse_type').append('<li><span>'+item.className+'</span><i class="fa fa-trash"></i></li>')
        }
      }

      $('ul#parse_type li span').click(refresh_table);
      $('ul#parse_type li i').click(delete_table);
    });

    ParseSDK.call('GET','users',null,function (data) {
      var item;

      for (var i=0 ; i<data.results.length ; i++) {
        item = data.results[i];

        $('#parse_user').append('<li>'+item+'</li>')
      }
    });

    ParseSDK.call('GET','roles',null,function (data) {
      var item;

      for (var i=0 ; i<data.results.length ; i++) {
        item = data.results[i];

        $('#parse_role').append('<li>'+item+'</li>')
      }
    });

    $('#create_type').click(function (ev) {
        $('#modalParseType').modal('show');
    });
    $('#parseTypeField').keypress(function (ev) {
        var candid = document.getElementById("parseTypeField");

        var fields = [
          'Boolean','Number','String','Date','DateTime',
          'File','Pointer','Relation',
        ];

        if (ev.keyCode==13) {
            var resp = '<div class="col-md-6"><div class="mb-3">';

            resp += '<input type="text" class="form-control" name="parseFieldName"';

            resp += 'aria-describedby="parseFieldNameHelp" value="'+candid.value+'">';

            resp += '</div></div><div class="col-md-6"><div class="mb-3">';

            resp += '<select class="form-select" name="parseFieldType">';

            for (i=0 ; i<fields.length ; i++) {
              resp += '<option value="'+fields[i]+'">'+fields[i]+'</option>';
            }

            resp += '</select></div></div>';

            $('#parseTypeList').append(resp);

            candid.value = '';
        }
    });
    $('#create_type_btn').click(function (ev) {
        var key=$('#parseTypeName').val(), sch={}, lst=$('input[name="parseFieldName"').length;

        for (i=0 ; i<lst ; i++) {
          col = $('input[name="parseFieldName"]')[i].value;
          dat = $('select[name="parseFieldType"]')[i].value;

          sch[col] = { "type": dat };
        }

        ParseSDK.call('POST','schemas/'+key,{
          "fields": sch,
          "className": key,
        },function (data) {
          $('#parse_type').append('<li><span>'+key+'</span><i class="fa fa-trash"></i></li>')

          $('ul#parse_type li span').click(refresh_table);
          $('ul#parse_type li i').click(delete_table);

          $('#modalParseType').modal('hide');
        });
    });

    $('#tool_backup').click(Parse.save);
});

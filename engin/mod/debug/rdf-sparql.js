var name = window.location.search.replace('?name=',''); // Endpoint.args.name;

var path, base = 'http://localhost:4747';

switch (name) {
  case 'hylar':
      path = "/model/lod/sparql";
      break;
  case 'quadstore':
      path = "/nerve/lod/sparql";
      break;
  case 'trifid':
      path = "/daten/lod/sparql";
      break;
  default:
      path = "/sparql";
}

link = base + path;

const yasgui = new Yasgui(document.getElementById("console"), {
  /**
   * Change the default request configuration, such as the headers
   * and default yasgui endpoint.
   * Define these fields as plain values, or as a getter function
   */
  requestConfig: {
    endpoint: link,
    //Example of using a getter function to define the headers field:
    headers: () => ({
      //'key': 'value'
    }),
    method: 'POST',


  },
  // Allow resizing of the Yasqe editor
  resizeable: true,

  // Whether to autofocus on Yasqe on page load
  autofocus: true,

  // Use the default endpoint when a new tab is opened
  copyEndpointOnNewTab: false,

  // Configuring which endpoints appear in the endpoint catalogue list
  endpointCatalogueOptions: {
    getData: () => {
      return [
        {
          endpoint: base + "/model/lod/sparql",
        },
        {
          endpoint: base + "/daten/lod/sparql",
        },
        {
          endpoint: base + "/nerve/lod/sparql",
        },
        //List of objects should contain the endpoint field
        //Feel free to include any other fields (e.g. a description or icon
        //that you'd like to use when rendering)
        {
          endpoint: "https://dbpedia.org/sparql"
        },
        {
          endpoint: "https://query.wikidata.org"
        }
        /*
        <option value="/model/lod/sparql">Hylar</option>
        <option value="/nerve/lod/query">QuadStore</option>
        <option value="/daten/lod/sparql">Trifid</option>
        <option value="/brain/bag/sparql">LD-Fragment</option>
        <option value="/brain/lod/sparql">Solid-LdP</option>
        //*/
      ];
    },
    //Data object keys that are used for filtering. The 'endpoint' key already used by default
    keys: [],
    //Data argument contains a `value` property for the matched data object
    //Source argument is the suggestion DOM element to append your rendered item to
    renderItem: (data, source) => {
        const contentDiv = document.createElement("div");
        contentDiv.innerText = data.value.endpoint;
        source.appendChild(contentDiv);
      }
  }
});

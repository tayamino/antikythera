jQuery(function ($) {
    var link = JSON.parse(localStorage['endpoint'] || '{}');;

    if (!(link.host)) link.host = 'localhost';
    if (!(link.port)) link.port = 4747;
    if (!(link.type)) link.type = 'http';

    //$('#viewer').attr('src',link.type+'://'+link.host+':'+link.port+'/brain/api');
});
// create the editor
const container = document.getElementById("editor")
const options = {}
const editor = new JSONEditor(container, options)

// set json
const initialJson = {
    "Array": [1, 2, 3],
    "Boolean": true,
    "Null": null,
    "Number": 123,
    "Object": {"a": "b", "c": "d"},
    "String": "Hello World"
}
editor.set(initialJson)

// get json
const updatedJson = editor.get()

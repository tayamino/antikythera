jQuery(function ($) {
    var link = JSON.parse(localStorage['endpoint'] || '{}');;

    if (!(link.host)) link.host = 'localhost';
    if (!(link.port)) link.port = 4747;
    if (!(link.type)) link.type = 'http';

    $('#viewer').attr('src',link.type+'://'+link.host+':'+link.port+'/nerve/api');
});

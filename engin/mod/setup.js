var mapping = {
    country: [
      { flag: 'ma', name: 'Morocco' },
      { flag: 'en', name: 'England' },
      { flag: 'us', name: 'United States' },
      { flag: 'fr', name: 'France' },
      { flag: 'sp', name: 'Spain' },
      { flag: 'pt', name: 'Portuguese' },
      { flag: 'de', name: 'Germany' },
    ],
    finance: [
        { code: 'MAD', flag: 'ma', name: "Moroccan Dirham" },
        { code: 'USD', flag: 'us', name: "U.S.A dollar" },
        { code: 'EUR', flag: 'eu', name: "European devise" },
        { code: 'YEN', flag: 'ja', name: "Japanese Yen" },
    ],
    langage: ['arabic','french','english','german','spanish','italian','russian','chinese'],
};

$(function () {
    for (i=0 ; i<mapping.country.length ; i++) {
        r = '<option value="'+mapping.country[i].flag+'">'+mapping.country[i].name+'</option>';

        $('select[data-config="perso_where_key"]').append(r);
    }

    for (i=0 ; i<mapping.langage.length ; i++) {
        t = mapping.langage[i][0].toUpperCase() + mapping.langage[i].substr(1);

        r = '<option value="'+mapping.langage[i]+'">'+t+'</option>';

        $('select[data-config="perso_where_say"]').append(r);

        $('select[data-config="perso_dialect"]').append(r);
    }

    for (i=0 ; i<mapping.finance.length ; i++) {
        t = mapping.finance[i].name[0].toUpperCase() + mapping.finance[i].name.substr(1);

        r = '<option value="'+mapping.finance[i].code+'">'+t+'</option>';

        $('select[data-config="perso_where_pay"]').append(r);

        $('select[data-config="perso_finance"]').append(r);
    }

    $("select[data-config]").change(function (ev) {
        var k = $(ev.currentTarget).attr('data-config');

        var v = $(ev.currentTarget).val();

        localStorage[k] = JSON.stringify(v);
    });
});

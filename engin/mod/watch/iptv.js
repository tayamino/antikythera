function iptv_refresh () {
    $.ajax({
        success: function (data) {
            data = data.replace("#EXTM3U \n#EXTINF:-1,","");

            data = data.split('\n#EXTINF:-1,');

            for (var i=0 ; i<data.length ; i++) {
                item = data[i].split('\n');

                flag = item[1].replace('channels/','').replace('.m3u','');

                flag = '<span class="flag-icon flag-icon-'+flag+' flag-icon-squared"></span>';

                $("ul#country").append('<li data-feed="'+item[1]+'">'+flag+'  '+item[0]+'</li>');
            }

            $("ul#country li").click(iptv_selects);
        },
        url: '/watch/m3u8/iptv.org/index.m3u',
    });
}

function iptv_selects (ev) {
    var link = $(ev.currentTarget).attr('data-feed');

    $.ajax({
        success: function (data) {
            data = data.replace("#EXTM3U","");

            $("ul#channel li").remove();

            data = data.split('\n#EXTINF:-1 ');

            for (var i=0 ; i<data.length ; i++) {
                item = data[i].split('\n');

                item[0] = item[0].split(',');

                if (item.length==2 && item[0].length==2) {
                    item = {
                        name: item[0][1],
                        link: item[1],
                        data: item[0][0],
                        attr: {},
                    };

                    var resp = item.data.split('" ');

                    for (var i=0 ; i<resp.length ; i++) {
                        resp[i] = resp[i].split('="');

                        item.attr[resp[i][0]] = resp[i][1];
                    }

                    console.log(item);
                    
                    $("ul#channel").append('<li data-attr="'+item.data+'" data-feed="'+item.link+'">'+item.name+'</li>');
                }
            }

            $("ul#channel li").click(iptv_channel);
        },
        url: '/watch/m3u8/iptv.org/'+link,
    });
}

function iptv_channel (ev) {
    var link = $(ev.currentTarget).attr('data-feed');

    var resp = '<video id=example-video width=640 height=480 class="video-js vjs-default-skin" controls>';

    resp += '<source src="'+link+'" type="application/x-mpegURL">';

    resp += '</video>';

    $('#player').html(resp);

    lecteur = videojs('example-video');

    lecteur.play();
}

var lecteur = null;

jQuery(function ($) {
    iptv_refresh();
});


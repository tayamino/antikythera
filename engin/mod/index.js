function read_endpoint() {
    cfg = JSON.parse(localStorage['endpoint'] || '{}');
    
    if (!(cfg.host)) cfg.host = 'localhost';
    if (!(cfg.port)) cfg.port = 3000;
    if (!(cfg.type)) cfg.type = 'http';

    $('#endpointHost').val(cfg.host);
    $('#endpointPort').val(cfg.port);
    $('#endpointType').val(cfg.type);
}
function save_endpoint() {
    cfg = {
        host: $('#endpointHost').val() || 'localhost',
        port: $('#endpointPort').val() || 3000,
        type: $('#endpointType').val() || 'http',
    };

    cfg.link = cfg.type+'://'+cfg.host+':'+cfg.port;

    localStorage['endpoint'] = JSON.stringify(cfg);
}

jQuery(function ($) {
    $('button#saving').click(save_endpoint);

    $('#endpointForm input,#endpointForm select').keypress(save_endpoint);
    $('#endpointForm input,#endpointForm select').change(save_endpoint);
    
    read_endpoint();
});

/* globals Chart:false, feather:false */

(function () {
  'use strict'

  feather.replace()

  // Graphs
  var ctx = document.getElementById('myChart')
  // eslint-disable-next-line no-unused-vars
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ],
      datasets: [{
        data: [
          15339,
          21345,
          18483,
          24003,
          23489,
          24092,
          12034
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#007bff',
        borderWidth: 4,
        pointBackgroundColor: '#007bff'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: false
      }
    }
  })
})()

Endpoint.ajax('back', "", function (data) {
    var tool = [{
        name: 'danger', icon: 'trash', text: "Delete"
    },{
        name: 'warning', icon: 'pencil', text: "Edit"
    },{
        name: 'success', icon: 'code', text: "Execute"
    },{
        name: 'dark', icon: 'recycle', text: "Initialize"
    },{
        name: 'primary', icon: 'upload', text: "Uploader"
    },{
        name: 'secondary', icon: 'download', text: "Download"
    }];
    //var name = ['uuid','type','host','port','user','pass','path','args','hash'];
    var name = ['uuid','type','host','port','path'];

    for (var i=0 ; i<data.length ; i++) {
        block  = '<th id="'+data[i].uuid+'" scope="row">'+(i+1)+'</th>';

        for (var j=0 ; j<name.length ; j++) {
            block += '<td>'+data[i][name[j]]+'</td>';
        }
        block += '<td>';
        for (var j=0 ; j<tool.length ; j++) {
            block += '<span class="tool btn btn-sm btn-'+tool[j].name+'"><i class="fa fa-'+tool[j].icon+'"></i></span>';
        }
        block += '</td>';

        $('#lister').append('<tr>'+block+'</tr>');
    }
    /**************************************************************************/
    $('span.tool.btn-danger').click(function (ev) {
        if (confirm('Do you really want to delete ?')) {
            console.log("Delete backend : ",uid);
        }
    });
    $('span.tool.btn-warning').click(function (ev) {
        $('#modalEditor').modal('show');
    });
    $('span.tool.btn-success').click(function (ev) {
        $('#modalCommand').modal('show');
    });
    $('span.tool.btn-dark').click(function (ev) {
        if (confirm('Do you really want to reset ?')) {
            console.log("Reset backend : ",uid);
        }
    });
    /**************************************************************************/
    $('span.tool.btn-primary').click(function (ev) {
        $('#modalLoader').modal('show');
    });
    $('span.tool.btn-secondary').click(function (ev) {
        $('#modalDumper').modal('show');
    });
    /**************************************************************************/
    $('#tools_import').click(function (ev) {
        $('#modalImport').modal('show');
    });
    $('#perform_import').click(function (ev) {
        $.ajax({
            data: {
                name: $('#backendName').val(),
                link: $('#backendLink').val(),
            },
            method: 'POST',
            url: Endpoint.conn.link+"/?acte=back",
        });
        $('#modalImport').modal('show');
    });
});


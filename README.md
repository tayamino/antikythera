CSP :
=====

default-src 'self'; connect-src * data: blob: filesystem:;

script-src 'self' 'unsafe-eval' filesystem:;
object-src 'self' 'unsafe-eval' filesystem:;

style-src 'self' data: 'unsafe-inline';
img-src 'self' data:;
frame-src 'self' data:;
font-src 'self' data:;
media-src * data: blob: filesystem:;


var Language = function (code,name,auto,flag) {
    this.code = code;
    this.name = name;
    this.auto = auto || name;
    this.flag = flag || code;
};
Language.prototype.wikition = function () {

};
Language.prototype.hunspell = function () {

};
Language.prototype.stanford = function () {

};
Language.prototype.wink_nlp = function () {

};
//##############################################################################
var Taxonomy = function (lang,slug,hook) {
    this.lang = lang;
    this.slug = slug;
    this.hook = hook;
};
Taxonomy.prototype.checker = function (value) {
    return this.hook(value.toLowerCase());
};
Taxonomy.prototype.process = function (value,objet) {
    return {
        name: value,
        type: this.slug.toUpperCase(),
    };
};
Taxonomy.prototype.stanford = function () {

};
Taxonomy.prototype.wink_nlp = function () {

};
//##############################################################################
var Concept = function () {

};
Concept.prototype.stanford_corenlp = function () {

};
Concept.prototype.wink_nlp_js = function () {

};
//##############################################################################
var Natural = {
    feel: function (value) {
        return Math.floor(Math.random() * 5) - 2;
    },
    stce: function (value) {
        var stce = value.split('.');

        var info = {
            source: value,
            tagged: value,
            metric: {
                word: 0,
                freq: [],
                freq: [],
            },
            entity: [],
            phrase: [],
        };

        var word;

        for (var i=0 ; i<stce.length ; i++) {
            info.phrase.push({
                sentence: stce[i],
                sentiment: Natural.feel(stce[i]),
            });

            word = stce[i].split(' ');

            for (var j=0 ; j<word.length ; j++) {
                var resp = null;

                for (var k=0 ; k<Natural.tags.length ; k++) {
                    var item = Natural.tags[k].checker(word[j]);

                    if (resp==null && item) {
                        resp = Natural.tags[k].process(word[j],item);
                    }
                }

                if (resp!=null) {
                    info.entity.push(resp);
                }
            }

            info.metric.word += word.length;
        }

        for (var i=0 ; i<info.entity.length ; i++) {
            info.tagged = info.tagged.replace(info.entity[i].name, '<span class="tag '+info.entity[i].type+'">'+info.entity[i].name+'</span>');
        }

        return info;

        const patterns = [
          { name: 'nounPhrase', patterns: [ '[|DET] [|ADJ] [NOUN|PROPN]' ] }
        ];

        nlp.learnCustomEntities( patterns );

        const doc = nlp.readDoc( text );

        doc.customEntities().out();

        console.log( 'Entities in Sentence No.: 4' );
        
        const sentence = doc.sentences().itemAt( 3 );

        console.log( sentence.entities().out() );    },
    lang: [],
    tags: [],
};

Natural.language = function (code,name,flag) {
    var resp = new Language(code,name,flag);

    Natural.lang.push(resp);

    return resp;
};

Natural.taxonomy = function (lang,slug,hook) {
    var resp = new Taxonomy(lang,slug,hook);

    Natural.tags.push(resp);

    return resp;
};

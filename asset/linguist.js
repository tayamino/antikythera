Natural.language('en', "english", "English");
Natural.language('fr', "french",  "Français");
Natural.language('es', "spanish", "Español");
Natural.language('de', "german",  "Deutsch");
Natural.language('ru', "russian", "русский");
Natural.language('zn', "chinese", "中文");
/***********************************************************************************/
Natural.language('it', "italian",    "Italiano");
Natural.language('nl', "dutch",      "Nederlands");
Natural.language('pt', "portuguese", "Português");
Natural.language('ja', "japanese",   "日本語");
/***********************************************************************************/
Natural.taxonomy(null,'percent',function (value) {
    if (value[value.length-1]=='%') {
        return true;
    }

    return false;
});
/***********************************************************************************/
Natural.taxonomy(null,'hashtag',function (value) {
    if (value[0]=='#') {
        return true;
    }

    return false;
});
/***********************************************************************************/
Natural.taxonomy(null,'money',function (value) {
    var curr = [
        '$',
        'USD',
        'MAD',
    ];

    for (var i=0 ; i<curr.length ; i++) {
        if (value.indexOf(curr[i]) > -1) {
            return true;
        }
    }

    return false;
});
/***********************************************************************************/
Natural.taxonomy(null,'number',function (value) {
    var resp;

    try {
        resp = parseFloat(value);
    } catch (ex) {
        resp = null;
    }

    return resp;
});
/***********************************************************************************/
Natural.taxonomy(null,'date',function (value) {
    return moment(value).isValid();
});
/***********************************************************************************/
Natural.taxonomy(null,'url',function (value) {
    var resp;

    try {
        resp = new URL(value);
    } catch (ex) {
        resp = null;
    }

    return resp;
});
/***********************************************************************************/
Natural.taxonomy(null,'ordinal',function (value) {
    var data = [
        'first',
        'second',
        'third',
        'forth',
        'fifth',
        'sixth',
        'seventh',
        'eighth',
        'nineth',
    ];

    for (var i=0 ; i<data.length ; i++) {
        if (data[i]==value) {
            return true;
        }
    }

    return false;
});
